<?php
/**
 * Name:  Header style 03
 **/
?>
<?php
$data_meta = get_post_meta(get_the_ID(), '_custom_metabox_theme_options', true);

$header_color = Furgan_Functions::furgan_get_option('header_color', 'header-dark');
$header_color = isset($data_meta['enable_header']) && $data_meta['enable_header'] == 1 && isset($data_meta['metabox_header_color']) && $data_meta['metabox_header_color'] != '' ? $data_meta['metabox_header_color'] : $header_color;

$enable_header_transparent = Furgan_Functions::furgan_get_option('enable_header_transparent');
$enable_header_transparent = isset($data_meta['enable_header']) && $data_meta['enable_header'] == 1 && isset($data_meta['metabox_enable_header_transparent']) ? $data_meta['metabox_enable_header_transparent'] : $enable_header_transparent;

$class = array('header', 'style-03', $header_color);

if (!is_single() && $enable_header_transparent == 1) {
    $class[] = 'header-transparent';
}
?>
<header id="header" class="<?php echo esc_attr(implode(' ', $class)); ?>">
    <div class="header-middle">
        <div class="container">
            <div class="header-middle-inner">
                <div class="header-search-wrap">
                    <div class="header-search">
                        <?php furgan_search_form(); ?>
                    </div>
                </div>
                <div class="header-logo-menu">
                    <div class="header-logo">
                        <?php furgan_get_logo(); ?>
                    </div>
                    <?php if (has_nav_menu('primary')) { ?>
                        <div class="box-header-nav">
                            <?php
                            wp_nav_menu(array(
                                    'menu' => 'primary',
                                    'theme_location' => 'primary',
                                    'depth' => 3,
                                    'container' => '',
                                    'container_class' => '',
                                    'container_id' => '',
                                    'menu_class' => 'furgan-nav main-menu horizontal-menu',
                                    'megamenu_layout' => 'horizontal',
                                )
                            );
                            ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="header-control">
                    <div class="header-control-inner">
                        <div class="meta-woo">
                            <?php
                            furgan_user_link();
                            do_action('furgan_header_mini_cart');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-mobile-wrap">
        <div class="container">
            <div class="header-mobile">
                <div class="header-mobile-left">
                    <?php if (has_nav_menu('primary')) { ?>
                        <div class="block-menu-bar">
                            <a class="menu-bar menu-toggle" href="javascript:void(0)">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                    <?php } ?>
                    <div class="header-search furgan-dropdown">
                        <div class="header-search-inner" data-furgan="furgan-dropdown">
                            <a href="javascript:void(0)" class="link-dropdown block-link">
                                <span class="flaticon-magnifying-glass-1"></span>
                            </a>
                        </div>
                        <?php furgan_search_form(); ?>
                    </div>
                </div>
                <div class="header-mobile-mid">
                    <div class="header-logo">
                        <?php furgan_get_logo(); ?>
                    </div>
                </div>
                <div class="header-mobile-right">
                    <div class="header-control-inner">
                        <div class="meta-woo">
                            <?php
                            furgan_user_link();
                            do_action('furgan_header_mini_cart');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
