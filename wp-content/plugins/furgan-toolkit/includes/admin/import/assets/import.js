;(function ($) {
    "use strict";

    var kt_import_percent = 0,
        kt_import_percent_increase = 0,
        kt_import_index_request = 0,
        kt_arr_import_request_data = [],
        optionid = '';

    $(document).on('click', '.button-primary.open-import', function () {
        var _contentID = $(this).data('id');
        tb_show('Import Option', '#TB_inline?inlineId=content-demo-' + _contentID + '');
    });

    function kt_import_ajax_handle() {
        if (kt_import_index_request == kt_arr_import_request_data.length) {
            $('#option-' + optionid).addClass('done-import');
            $('[data-option="' + optionid + '"]').find('.progress').hide();
            $('[data-option="' + optionid + '"]').find('.progress-wapper').addClass('complete');
            return;
        }
        $('[data-option="' + optionid + '"] .progress-item').find('.' + kt_arr_import_request_data[kt_import_index_request]["action"]).show();

        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: kt_arr_import_request_data[kt_import_index_request],
            complete: function (jqXHR, textStatus) {
                $('[data-option="' + optionid + '"] .progress-item').find('.' + kt_arr_import_request_data[kt_import_index_request]["action"]).addClass('complete');
                kt_import_percent += kt_import_percent_increase;
                kt_progress_bar_handle();
                kt_import_index_request++;
                setTimeout(function () {
                    kt_import_ajax_handle();
                }, 200);
            }
        });
    }

    function kt_progress_bar_handle() {

        if (kt_import_percent > 100) {
            kt_import_percent = 100;
        }
        var progress_bar = $('[data-option="' + optionid + '"]').find('.progress-circle .c100'),
            class_percent = 'p' + Math.ceil(kt_import_percent);
        progress_bar.addClass(class_percent);

        progress_bar.find('.percent').html(Math.ceil(kt_import_percent) + '%');
    }

    $(document).on('click', '.kt-button-import', function () {
        $(this).closest('#TB_ajaxContent').find('.progress-wapper').show();

        var id = $(this).data('id'),
            slug = $(this).data('slug'),
            content_ajax = $(this).closest('#TB_ajaxContent');

        content_ajax.find('[data-percent="1"]').attr('class', 'c100 p0 dark green');
        content_ajax.find('.percent').html('0%');
        content_ajax.find('.progress-wapper').show();
        kt_import_percent = 0;
        kt_import_percent_increase = 0;
        kt_import_index_request = 0;
        kt_arr_import_request_data = [];
        optionid = $(this).data('optionid');

        $('[data-option="' + optionid + '"]').find('.progress-wapper .item').removeClass('complete').css('display', 'none');

        // Demo content
        kt_arr_import_request_data.push({
            'action': 'kt_import_theme_options',
            'optionid': optionid
        });

        kt_arr_import_request_data.push({
            'action': 'kt_import_widget',
            'optionid': optionid
        });

        kt_arr_import_request_data.push({
            'action': 'kt_import_revslider',
            'optionid': optionid
        });

        kt_arr_import_request_data.push({
            'action': 'kt_import_config',
            'optionid': optionid
        });

        var total_ajaxs = kt_arr_import_request_data.length;

        if (total_ajaxs == 0) {
            return;
        }

        kt_import_percent_increase = (100 / total_ajaxs);

        kt_import_ajax_handle();
    });

})(jQuery, window, document);