<?php
$classes = array('post-item');
$classes[] = 'col-bg-' . Furgan_Functions::furgan_get_option('blog_bg_items', 4);
$classes[] = 'col-lg-' . Furgan_Functions::furgan_get_option('blog_lg_items', 4);
$classes[] = 'col-md-' . Furgan_Functions::furgan_get_option('blog_md_items', 4);
$classes[] = 'col-sm-' . Furgan_Functions::furgan_get_option('blog_sm_items', 6);
$classes[] = 'col-xs-' . Furgan_Functions::furgan_get_option('blog_xs_items', 12);
$classes[] = 'col-ts-' . Furgan_Functions::furgan_get_option('blog_ts_items', 12);
if (have_posts()) : ?>
    <div class="blog-grid auto-clear row equal-container better-height">
        <?php while (have_posts()) : the_post(); ?>
            <article <?php post_class($classes); ?>>
                <div class="post-grid post-inner">
                    <div class="post-thumb">
                        <a href="<?php the_permalink(); ?>">
                            <?php $thumb = apply_filters('furgan_resize_image', get_post_thumbnail_id(), 370, 300, true, true);
                            echo wp_specialchars_decode($thumb['img']); ?>
                        </a>
                        <?php furgan_post_datebox(); ?>
                    </div>
                    <div class="post-info">
                        <div class="post-meta">
                            <?php furgan_post_author(); ?>
                        </div>
                        <?php furgan_post_title(); ?>
                        <div class="post-content equal-elem">
                            <?php echo wp_trim_words(apply_filters('the_excerpt', get_the_excerpt()), 15, ''); ?>
                        </div>
                        <?php furgan_post_readmore();?>
                    </div>
                </div>
            </article>
        <?php endwhile;
        wp_reset_postdata(); ?>
    </div>
    <?php
    /**
     * Functions hooked into furgan_after_blog_content action
     *
     * @hooked furgan_paging_nav               - 10
     */
    do_action('furgan_after_blog_content'); ?>
<?php else :
    get_template_part('content', 'none');
endif;