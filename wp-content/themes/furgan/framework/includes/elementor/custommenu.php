<?php
if (!class_exists('Furgan_Elementor_Custommenu')) {
    class Furgan_Elementor_Custommenu extends Furgan_Elementor
    {
        public $name = 'custommenu';
        public $title = 'Custommenu';
        public $icon = 'furgan-elementor-icon eicon-anchor';

        /**
         * Register the widget controls.
         *
         * Adds different input fields to allow the user to change and customize the widget settings.
         *
         * @since 1.0.0
         *
         * @access protected
         */
        protected function register_controls()
        {

            $this->start_controls_section(
                'general_section',
                [
                    'label' => esc_html__('General', 'furgan'),
                ]
            );
            $this->add_control(
                'style',
                [
                    'label' => esc_html__('Style', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => Furgan_Elementor::furgan_elementor_preview($this->name),
                    'default' => 'style-01',
                    'label_block' => true
                ]
            );
            foreach (Furgan_Elementor::furgan_elementor_preview($this->name) as $key => $value) {
                $this->add_control(
                    $key,
                    [
                        'label' => '<img alt="' . esc_attr($this->name) . '" src="' . esc_url(get_theme_file_uri('/assets/images/elementor/' . $this->name . '/' . $key . '.jpg')) . '"/>',
                        'type' => \Elementor\Controls_Manager::HEADING,
                        'condition' => array(
                            'style' => $key
                        ),
                    ]
                );
            };
            $this->add_responsive_control(
                'align',
                [
                    'label' => esc_html__('Alignment', 'furgan'),
                    'type' => \Elementor\Controls_Manager::CHOOSE,
                    'options' => [
                        'left' => [
                            'title' => esc_html__('Left', 'furgan'),
                            'icon' => 'eicon-text-align-left',
                        ],
                        'center' => [
                            'title' => esc_html__('Center', 'furgan'),
                            'icon' => 'eicon-text-align-center',
                        ],
                        'right' => [
                            'title' => esc_html__('Right', 'furgan'),
                            'icon' => 'eicon-text-align-right',
                        ],
                        'justify' => [
                            'title' => esc_html__('Justified', 'furgan'),
                            'icon' => 'eicon-text-align-justify',
                        ],
                    ],
                    'selectors' => [
                        '{{WRAPPER}}' => 'text-align: {{VALUE}};',
                    ],
                ]
            );
            $this->add_control(
                'title', [
                    'label' => esc_html__('Title', 'furgan'),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'placeholder' => esc_html__('Enter your title', 'furgan'),
                    'condition' => array(
                        'style' => array('style-01', 'style-04', 'style-05', 'style-06')
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'nav_menu',
                [
                    'label' => esc_html__('Menu', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => Furgan_Elementor::furgan_elementor_menu(),
                    'description' => esc_html__('Select menu to display.', 'furgan'),
                    'label_block' => true
                ]
            );
            $this->end_controls_section();

        }

        /**
         * Render the widget output on the frontend.
         *
         * Written in PHP and used to generate the final HTML.
         *
         * @since 1.0.0
         *
         * @access protected
         */
        protected function render()
        {
            $atts = $this->get_settings_for_display();
            $css_class = array('furgan-custommenu');
            $css_class[] = $atts['style'];
            $type = 'WP_Nav_Menu_Widget';
            $args = array();
            global $wp_widget_factory;
            ?>
            <div class="<?php echo esc_attr(implode(' ', $css_class)); ?>">
                <?php
                if (is_object($wp_widget_factory) && isset($wp_widget_factory->widgets, $wp_widget_factory->widgets[$type])) {
                    the_widget($type, $atts, $args);
                } else { ?>
                    <p><?php esc_html_e('No Content', 'furgan'); ?></p>
                <?php } ?>
            </div>
            <?php
        }
    }
}