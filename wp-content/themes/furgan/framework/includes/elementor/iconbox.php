<?php
if (!class_exists('Furgan_Elementor_Iconbox')) {
    class Furgan_Elementor_Iconbox extends Furgan_Elementor
    {
        public $name = 'iconbox';
        public $title = 'Iconbox';
        public $icon = 'furgan-elementor-icon eicon-icon-box';

        /**
         * Register the widget controls.
         *
         * Adds different input fields to allow the user to change and customize the widget settings.
         *
         * @since 1.0.0
         *
         * @access protected
         */
        protected function register_controls()
        {

            $this->start_controls_section(
                'general_section',
                [
                    'label' => esc_html__('General', 'furgan'),
                ]
            );
            $this->add_control(
                'style',
                [
                    'label' => esc_html__('Style', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => Furgan_Elementor::furgan_elementor_preview($this->name),
                    'default' => 'style-01',
                    'label_block' => true
                ]
            );
            foreach (Furgan_Elementor::furgan_elementor_preview($this->name) as $key => $value) {
                $this->add_control(
                    $key,
                    [
                        'label' => '<img alt="' . esc_attr($this->name) . '" src="' . esc_url(get_theme_file_uri('/assets/images/elementor/' . $this->name . '/' . $key . '.jpg')) . '"/>',
                        'type' => \Elementor\Controls_Manager::HEADING,
                        'condition' => array(
                            'style' => $key
                        ),
                    ]
                );
            }
            $this->add_control(
                'type', [
                    'label' => esc_html__('Icon library', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        'media' => esc_html__('Media', 'furgan'),
                        'icon' => esc_html__('Icon', 'furgan'),
                    ],
                    'default' => 'media',
                    'label_block' => true
                ]
            );
            $this->add_control(
                'media', [
                    'label' => esc_html__('Media', 'furgan'),
                    'type' => \Elementor\Controls_Manager::ICONS,
                    'default' => [
                        'value'   => 'fas fa-star',
                        'library' => 'fa-solid',
                    ],
                    'condition' => array(
                        'type' => array('media')
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'icon', [
                    'label' => esc_html__('Icon', 'furgan'),
                    'type' => \Elementor\Controls_Manager::ICON,
                    'options' => Furgan_Elementor::furgan_elementor_icon(),
                    'default' => 'fa fa-star',
                    'condition' => array(
                        'type' => array('icon')
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'title', [
                    'label' => esc_html__('Title', 'furgan'),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'placeholder' => esc_html__('Enter your title', 'furgan'),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'desc', [
                    'label' => esc_html__('Description', 'furgan'),
                    'type' => \Elementor\Controls_Manager::TEXTAREA,
                    'placeholder' => esc_html__('Enter your description', 'furgan'),
                    'label_block' => true
                ]
            );
            $this->end_controls_section();

        }

        /**
         * Render the widget output on the frontend.
         *
         * Written in PHP and used to generate the final HTML.
         *
         * @since 1.0.0
         *
         * @access protected
         */
        protected function render()
        {
            $atts = $this->get_settings_for_display();
            $css_class = array('furgan-iconbox');
            $css_class[] = $atts['style'];
            ?>
            <div class="<?php echo esc_attr(implode(' ', $css_class)); ?>">
                <div class="iconbox-inner">
                    <?php if ($atts['type'] == 'icon' && $atts['icon']): ?>
                        <div class="icon">
                            <span class="<?php echo esc_attr($atts['icon']) ?>"></span>
                            <?php if ($atts['style'] == 'style-01'): ?>
                                <span class="<?php echo esc_attr($atts['icon']) ?>"></span>
                            <?php endif; ?>
                        </div>
                    <?php elseif ($atts['type'] == 'media' && $atts['media']): ?>
                        <div class="icon">
                            <?php Elementor\Icons_Manager::render_icon($atts['media'], ['aria-hidden' => 'true']); ?>
                        </div>
                    <?php endif; ?>
                    <div class="content">
                        <?php if ($atts['title']): ?>
                            <h4 class="title"><?php echo esc_html($atts['title']); ?></h4>
                        <?php endif; ?>
                        <?php if ($atts['desc']): ?>
                            <div class="desc"><?php echo wp_specialchars_decode($atts['desc']); ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php
        }
    }
}