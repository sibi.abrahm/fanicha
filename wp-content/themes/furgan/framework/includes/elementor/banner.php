<?php
if (!class_exists('Furgan_Elementor_Banner')) {
    class Furgan_Elementor_Banner extends Furgan_Elementor
    {
        public $name = 'banner';
        public $title = 'Banner';
        public $icon = 'furgan-elementor-icon eicon-banner';

        /**
         * Register the widget controls.
         *
         * Adds different input fields to allow the user to change and customize the widget settings.
         *
         * @since 1.0.0
         *
         * @access protected
         */
        protected function register_controls()
        {

            $this->start_controls_section(
                'general_section',
                [
                    'label' => esc_html__('General', 'furgan'),
                ]
            );
            $this->add_control(
                'style',
                [
                    'label' => esc_html__('Style', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => Furgan_Elementor::furgan_elementor_preview($this->name),
                    'default' => 'style-01',
                    'label_block' => true
                ]
            );
            foreach (Furgan_Elementor::furgan_elementor_preview($this->name) as $key => $value) {
                $this->add_control(
                    $key,
                    [
                        'label' => '<img alt="' . esc_attr($this->name) . '" src="' . esc_url(get_theme_file_uri('/assets/images/elementor/' . $this->name . '/' . $key . '.jpg')) . '"/>',
                        'type' => \Elementor\Controls_Manager::HEADING,
                        'condition' => array(
                            'style' => $key
                        ),
                    ]
                );
            }
            $this->add_control(
                'image', [
                    'label' => esc_html__('Image', 'furgan'),
                    'type' => \Elementor\Controls_Manager::MEDIA,
                    'default' => [
                        'url' => \Elementor\Utils::get_placeholder_image_src(),
                    ],
                    'label_block' => true
                ]
            );
            $this->add_control(
                'cate', [
                    'label' => esc_html__('Label', 'furgan'),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'placeholder' => esc_html__('Enter your label', 'furgan'),
                    'condition' => array(
                        'style' => array('style-02', 'style-07', 'style-09')
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'align', [
                    'label' => esc_html__('Box Align', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => array(
                        'left-center' => esc_html__('Left Center', 'furgan'),
                        'left-top' => esc_html__('Left Top', 'furgan'),
                        'left-bottom' => esc_html__('Left Bottom', 'furgan'),
                        'right-top' => esc_html__('Right Top', 'furgan'),
                    ),
                    'default' => 'left-center',
                    'condition' => array(
                        'style' => array('style-01')
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'title', [
                    'label' => esc_html__('Title', 'furgan'),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'placeholder' => esc_html__('Enter your title', 'furgan'),
                    'rows' => 1,
                    'label_block' => true
                ]
            );
            $this->add_control(
                'subtitle', [
                    'label' => esc_html__('Subtitle', 'furgan'),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'placeholder' => esc_html__('Enter your subtitle', 'furgan'),
                    'condition' => array(
                        'style' => array('style-03', 'style-04', 'style-05', 'style-06', 'style-07', 'style-08', 'style-10', 'style-11'),
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'desc', [
                    'label' => esc_html__('Description', 'furgan'),
                    'type' => \Elementor\Controls_Manager::WYSIWYG,
                    'placeholder' => esc_html__('Enter your description', 'furgan'),
                    'condition' => array(
                        'style' => array('style-05', 'style-06', 'style-07', 'style-09', 'style-11'),
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'text', [
                    'label' => esc_html__('Button Text', 'furgan'),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'placeholder' => esc_html__('Enter your button text', 'furgan'),
                    'condition' => array(
                        'style' => array('style-02', 'style-04', 'style-05', 'style-06', 'style-07', 'style-08', 'style-09', 'style-10', 'style-11'),
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'link', [
                    'label' => esc_html__('Button Link', 'furgan'),
                    'type' => \Elementor\Controls_Manager::URL,
                    'placeholder' => esc_html__('https://your-link.com', 'furgan'),
                    'show_external' => true,
                    'default' => [
                        'url' => '',
                        'is_external' => true,
                        'nofollow' => false,
                    ],
                    'label_block' => true
                ]
            );
            $this->end_controls_section();

        }

        /**
         * Render the widget output on the frontend.
         *
         * Written in PHP and used to generate the final HTML.
         *
         * @since 1.0.0
         *
         * @access protected
         */
        protected function render()
        {
            $atts = $this->get_settings_for_display();
            $css_class = array('furgan-banner');
            $css_class[] = $atts['style'];
            if ($atts['style'] == 'style-01') {
                $css_class[] = $atts['align'];
            }
            $container = array('banner-info');
            $container[] = 'clearfix';
            if (in_array($atts['style'], array('style-02', 'style-07', 'style-09'))) {
                $container[] = 'container';
            }
            $target = $atts['link']['is_external'] ? '_blank' : '_self';
            ?>
            <div class="<?php echo esc_attr(implode(' ', $css_class)); ?>">
                <div class="banner-inner">
                    <?php if ($atts['image']): ?>
                        <figure class="banner-thumb">
                            <?php echo wp_get_attachment_image($atts['image']['id'], 'full'); ?>
                        </figure>
                    <?php endif; ?>
                    <div class="<?php echo esc_attr(implode(' ', $container)); ?>">
                        <div class="banner-content">
                            <?php if ($atts['cate'] && in_array($atts['style'], array('style-02', 'style-03', 'style-04', 'style-07', 'style-09', 'style-11'))): ?>
                                <div class="banner-label"><?php echo esc_html($atts['cate']); ?></div>
                            <?php endif; ?>
                            <?php if ($atts['title']): ?>
                                <h3 class="title">
                                    <?php if (in_array($atts['style'], array('style-01', 'style-03')) && $atts['link']['url']) { ?>
                                        <a href="<?php echo esc_url($atts['link']['url']); ?>"
                                           target="<?php echo esc_attr($target); ?>"><?php echo esc_html($atts['title']); ?></a>
                                    <?php } else {
                                        echo esc_html($atts['title']);
                                    } ?>
                                </h3>
                            <?php endif; ?>
                            <?php if ($atts['subtitle'] && in_array($atts['style'], array('style-03', 'style-04', 'style-05', 'style-06', 'style-07', 'style-08', 'style-10', 'style-11'))): ?>
                                <div class="subtitle"><?php echo esc_html($atts['subtitle']); ?></div>
                            <?php endif; ?>
                            <?php if ($atts['desc'] && in_array($atts['style'], array('style-05', 'style-06', 'style-07', 'style-09', 'style-11'))): ?>
                                <div class="desc"><?php echo wp_specialchars_decode($atts['desc']); ?></div>
                            <?php endif; ?>
                            <?php if ($atts['text'] && $atts['link']['url']): ?>
                                <a class="button" href="<?php echo esc_url($atts['link']['url']); ?>"
                                   target="<?php echo esc_attr($target); ?>"><?php echo esc_html($atts['text']); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }
}