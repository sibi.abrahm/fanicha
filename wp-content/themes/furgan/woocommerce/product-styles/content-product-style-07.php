<?php
/**
 * Name: Product style 07
 * Slug: content-product-style-07
 **/
remove_action( 'woocommerce_before_shop_loop_item_title', 'furgan_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'furgan_gallery_product_thumbnail', 10, 1 );
?>
<div class="product-inner" data-items="2">
    <div class="product-thumb">
        <?php
        /**
         * woocommerce_before_shop_loop_item_title hook.
         *
         * @hooked furgan_woocommerce_group_flash - 10
         * @hooked furgan_template_loop_product_thumbnail - 10
         */
        do_action( 'woocommerce_before_shop_loop_item_title' );
        ?>
    </div>
    <div class="product-info">
        <div class="product-info-inner">
            <h3 class="title">
                <?php echo esc_html__( 'Deals of the day', 'furgan' ); ?>
            </h3>
            <?php
            /**
             * woocommerce_shop_loop_item_title hook.
             *
             * @hooked furgan_template_loop_product_title - 10
             */
            do_action( 'woocommerce_shop_loop_item_title' );
            /**
             * woocommerce_after_shop_loop_item_title hook.
             *
             * @hooked woocommerce_template_loop_rating - 5
             * @hooked woocommerce_template_loop_price - 10
             */
            do_action( 'woocommerce_after_shop_loop_item_title' );
            do_action( 'furgan_function_shop_loop_process_variable' );
            do_action( 'furgan_function_shop_loop_item_countdown' );
            ?>
            <div class="add-to-cart">
                <?php
                /**
                 * woocommerce_after_shop_loop_item hook.
                 *
                 * @removed woocommerce_template_loop_product_link_close - 5
                 * @hooked woocommerce_template_loop_add_to_cart - 10
                 */
                do_action('woocommerce_after_shop_loop_item');
                ?>
            </div>
        </div>
    </div>
</div>
<?php
add_action( 'woocommerce_before_shop_loop_item_title', 'furgan_template_loop_product_thumbnail', 10 );
remove_action( 'woocommerce_before_shop_loop_item_title', 'furgan_gallery_product_thumbnail', 10 );