<?php
if (have_posts()) : ?>
    <?php do_action('furgan_before_blog_content'); ?>
    <div class="blog-standard content-post">
        <?php while (have_posts()) : the_post(); ?>
            <article <?php post_class('post-item'); ?>>
                <div class="post-standard post-inner">
                    <?php furgan_post_format(); ?>
                    <div class="post-info">
                        <?php
                        furgan_post_title();
                        ?>
                        <div class="post-meta">
                            <?php
                            furgan_post_author();
                            furgan_post_date();
                            furgan_post_comment();
                            ?>
                        </div>
                    </div>
                    <?php
                    furgan_post_excerpt();
                    furgan_post_readmore();
                    ?>
                </div>
            </article>
        <?php endwhile;
        wp_reset_postdata(); ?>
    </div>
    <?php
    /**
     * Functions hooked into furgan_after_blog_content action
     *
     * @hooked furgan_paging_nav               - 10
     */
    do_action('furgan_after_blog_content'); ?>
<?php else :
    get_template_part('content', 'none');
endif; ?>