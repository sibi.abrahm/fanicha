<?php
if (!class_exists('Furgan_Elementor_Products')) {
    class Furgan_Elementor_Products extends Furgan_Elementor
    {
        public $name = 'products';
        public $title = 'Products';
        public $icon = 'furgan-elementor-icon eicon-woocommerce';

        /**
         * Register the widget controls.
         *
         * Adds different input fields to allow the user to change and customize the widget settings.
         *
         * @since 1.0.0
         *
         * @access protected
         */
        protected function register_controls()
        {
            //General Section
            $this->start_controls_section(
                'general_section',
                [
                    'label' => esc_html__('General', 'furgan'),
                ]
            );
            $this->add_control(
                'style',
                [
                    'label' => esc_html__('Style', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => Furgan_Elementor::furgan_elementor_preview($this->name),
                    'default' => 'style-01',
                    'label_block' => true
                ]
            );
            foreach (Furgan_Elementor::furgan_elementor_preview($this->name) as $key => $value) {
                $this->add_control(
                    $key,
                    [
                        'label' => '<img alt="' . esc_attr($this->name) . '" src="' . esc_url(get_theme_file_uri('/assets/images/elementor/' . $this->name . '/' . $key . '.jpg')) . '"/>',
                        'type' => \Elementor\Controls_Manager::HEADING,
                        'condition' => array(
                            'style' => $key
                        ),
                    ]
                );
            }
            $this->add_control(
                'title', [
                    'label' => esc_html__('Title', 'furgan'),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'placeholder' => esc_html__('Enter your title', 'furgan'),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'desc', [
                    'label' => esc_html__('Description', 'furgan'),
                    'type' => \Elementor\Controls_Manager::TEXTAREA,
                    'placeholder' => esc_html__('Enter your description', 'furgan'),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'product_image_size',
                [
                    'label' => esc_html__('Image size', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => Furgan_Elementor::furgan_elementor_size(),
                    'description' => esc_html__('Select a size for product', 'furgan'),
                    'label_block' => true

                ]
            );
            $this->add_control(
                'product_custom_thumb_width',
                [
                    'label' => esc_html__('Width', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 1,
                    'default' => 300,
                    'description' => esc_html__( 'Unit px', 'furgan' ),
                    'condition' => array(
                        'product_image_size' => 'custom'
                    ),
                ]
            );
            $this->add_control(
                'product_custom_thumb_height',
                [
                    'label' => esc_html__('Height', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 1,
                    'default' => 300,
                    'description' => esc_html__( 'Unit px', 'furgan' ),
                    'condition' => array(
                        'product_image_size' => 'custom'
                    ),
                ]
            );
            $this->add_control(
                'target',
                [
                    'label' => esc_html__('Target', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        'recent-product' => esc_html__('Recent Products', 'furgan'),
                        'best-selling' => esc_html__('Best Selling Products', 'furgan'),
                        'top-rated' => esc_html__('Top Rated Products', 'furgan'),
                        'product-category' => esc_html__('Product Category', 'furgan'),
                        'featured_products' => esc_html__('Featured Products', 'furgan'),
                        'on_sale' => esc_html__('On Sale', 'furgan'),
                        'on_new' => esc_html__('On New', 'furgan'),
                        'products' => esc_html__('Products', 'furgan'),
                    ],
                    'default' => 'recent-product',
                    'description' => esc_html__('Choose the target to filter products', 'furgan'),
                    'label_block' => true

                ]
            );
            $this->add_control(
                'taxonomy', [
                    'label' => esc_html__('Product Category', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT2,
                    'multiple' => true,
                    'options' => Furgan_Elementor::furgan_elementor_taxonomy(),
                    'description' => esc_html__('Note: If you want to narrow output, select category(s) above. Only selected categories will be displayed.', 'furgan'),
                    'condition' => array(
                        'target' => array('recent-product', 'top-rated', 'product-category', 'featured_products', 'on_sale', 'on_new', 'product_attribute')
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'orderby',
                [
                    'label' => esc_html__('Order by', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        'date' => esc_html__('Date', 'furgan'),
                        'ID' => esc_html__('ID', 'furgan'),
                        'author' => esc_html__('Author', 'furgan'),
                        'title' => esc_html__('Title', 'furgan'),
                        'modified' => esc_html__('Modified', 'furgan'),
                        'rand' => esc_html__('Random', 'furgan'),
                        'comment_count' => esc_html__('Comment count', 'furgan'),
                        'menu_order' => esc_html__('Menu order', 'furgan'),
                        '_sale_price' => esc_html__('Sale price', 'furgan'),
                    ],
                    'default' => 'date',
                    'description' => esc_html__('Select how to sort', 'furgan'),
                    'condition' => array(
                        'target' => array('recent-product', 'top-rated', 'product-category', 'featured_products', 'on_sale', 'on_new', 'product_attribute')
                    ),
                    'label_block' => true

                ]
            );
            $this->add_control(
                'order',
                [
                    'label' => esc_html__('Order', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        'ASC' => esc_html__('Ascending', 'furgan'),
                        'DESC' => esc_html__('Descending', 'furgan'),
                    ],
                    'default' => 'DESC',
                    'condition' => array(
                        'target' => array('recent-product', 'top-rated', 'product-category', 'featured_products', 'on_sale', 'on_new', 'product_attribute')
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'per_page',
                [
                    'label' => esc_html__('Product per page', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 1,
                    'step' => 1,
                    'default' => 6,
                    'condition' => array(
                        'target' => array('recent-product', 'best-selling', 'top-rated', 'product-category', 'featured_products', 'on_sale', 'on_new', 'product_attribute')
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'ids',
                [
                    'label' => esc_html__('Product IDs', 'furgan'),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'placeholder' => esc_html__('Enter IDs', 'furgan'),
                    'description' => esc_html__('Ex: 1,2,3,...', 'furgan'),
                    'condition' => array(
                        'target' => 'products'
                    ),
                    'label_block' => true
                ]
            );
            $this->end_controls_section();
            //Layout Section
            $this->start_controls_section(
                'layout_section',
                [
                    'label' => esc_html__('Layout', 'furgan'),
                ]
            );
            $this->add_control(
                'layout',
                [
                    'label' => esc_html__('Layout', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        'carousel' => esc_html__('Carousel', 'furgan'),
                        'grid' => esc_html__('Grid', 'furgan'),
                    ],
                    'default' => 'carousel',
                    'label_block' => true
                ]
            );
            $this->add_control(
                'carousel',
                [
                    'label' => '<img alt="' . esc_attr($this->name) . '" src="' . esc_url(get_theme_file_uri('/assets/images/elementor/layout/carousel.png')) . '"/>',
                    'type' => \Elementor\Controls_Manager::HEADING,
                    'condition' => array(
                        'layout' => 'carousel'
                    ),
                ]
            );
            $this->add_control(
                'grid',
                [
                    'label' => '<img alt="' . esc_attr($this->name) . '" src="' . esc_url(get_theme_file_uri('/assets/images/elementor/layout/grid.png')) . '"/>',
                    'type' => \Elementor\Controls_Manager::HEADING,
                    'condition' => array(
                        'layout' => 'grid'
                    ),
                ]
            );
            $this->add_control(
                'owl_number_row',
                [
                    'label' => esc_html__('The Number Of Rows', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        '1' => esc_html__('1 Row', 'furgan'),
                        '2' => esc_html__('2 Rows', 'furgan'),
                        '3' => esc_html__('3 Rows', 'furgan'),
                        '4' => esc_html__('4 Rows', 'furgan'),
                        '5' => esc_html__('5 Rows', 'furgan'),
                        '6' => esc_html__('6 Rows', 'furgan'),
                    ],
                    'default' => '1',
                    'condition' => array(
                        'layout' => 'carousel'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_rows_space',
                [
                    'label' => esc_html__('Rows Space', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        'rows-space-0' => esc_html__('Default', 'furgan'),
                        'rows-space-10' => esc_html__('10px', 'furgan'),
                        'rows-space-20' => esc_html__('20px', 'furgan'),
                        'rows-space-30' => esc_html__('30px', 'furgan'),
                        'rows-space-40' => esc_html__('40px', 'furgan'),
                        'rows-space-50' => esc_html__('50px', 'furgan'),
                        'rows-space-60' => esc_html__('60px', 'furgan'),
                        'rows-space-70' => esc_html__('70px', 'furgan'),
                        'rows-space-80' => esc_html__('80px', 'furgan'),
                        'rows-space-90' => esc_html__('90px', 'furgan'),
                        'rows-space-100' => esc_html__('100px', 'furgan'),
                    ],
                    'default' => 'rows-space-0',
                    'condition' => array(
                        'layout' => 'carousel'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_vertical',
                [
                    'label' => esc_html__('Vertical Slide Mode', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        'true' => esc_html__('Yes', 'furgan'),
                        'false' => esc_html__('No', 'furgan'),
                    ],
                    'default' => 'false',
                    'condition' => array(
                        'layout' => 'carousel'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_verticalswiping',
                [
                    'label' => esc_html__('Vertical Swipe Mode', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        'true' => esc_html__('Yes', 'furgan'),
                        'false' => esc_html__('No', 'furgan'),
                    ],
                    'default' => 'false',
                    'condition' => array(
                        'layout' => 'carousel',
                        'owl_vertical' => 'true'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_autoplay',
                [
                    'label' => esc_html__('AutoPlay', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        'true' => esc_html__('Yes', 'furgan'),
                        'false' => esc_html__('No', 'furgan'),
                    ],
                    'default' => 'false',
                    'description' => esc_html__('Enables Autoplay', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_autoplayspeed',
                [
                    'label' => esc_html__('Autoplay Speed', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 1000,
                    'default' => 3000,
                    'description' => esc_html__('Autoplay Speed in milliseconds', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel',
                        'owl_autoplay' => 'true'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_navigation',
                [
                    'label' => esc_html__('Navigation', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        'true' => esc_html__('Yes', 'furgan'),
                        'false' => esc_html__('No', 'furgan'),
                    ],
                    'default' => 'true',
                    'description' => esc_html__('Prev/Next Arrows', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_dots',
                [
                    'label' => esc_html__('Dots', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        'true' => esc_html__('Yes', 'furgan'),
                        'false' => esc_html__('No', 'furgan'),
                    ],
                    'default' => 'false',
                    'description' => esc_html__('Show dot indicators', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_loop',
                [
                    'label' => esc_html__('Loop', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        'true' => esc_html__('Yes', 'furgan'),
                        'false' => esc_html__('No', 'furgan'),
                    ],
                    'default' => 'false',
                    'description' => esc_html__('Infinite loop sliding', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_slidespeed',
                [
                    'label' => esc_html__('Slide Speed', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 300,
                    'default' => 300,
                    'description' => esc_html__('Slide Speed in milliseconds', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_ls_items',
                [
                    'label' => esc_html__('Items per row on Wide Screens', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 1,
                    'step' => 1,
                    'default' => 4,
                    'description' => esc_html__('Items per row on screen resolution of device >= 1500px', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_slide_margin',
                [
                    'label' => esc_html__('Margin on Wide Screens', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 0,
                    'default' => 30,
                    'description' => esc_html__('Distance( or space) between 2 items on screen resolution of device >= 1500px', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel',
                        'owl_vertical' => 'false'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_lg_items',
                [
                    'label' => esc_html__('Items per row on Desktop', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 1,
                    'step' => 1,
                    'default' => 4,
                    'description' => esc_html__('Items per row on screen resolution of device >= 1200px and < 1500px', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_lg_slide_margin',
                [
                    'label' => esc_html__('Margin on Desktop', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 0,
                    'default' => 30,
                    'description' => esc_html__('Distance( or space) between 2 items on screen resolution of device >= 1200px and < 1500px', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel',
                        'owl_vertical' => 'false'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_md_items',
                [
                    'label' => esc_html__('Items per row on Landscape Tablet', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 1,
                    'step' => 1,
                    'default' => 3,
                    'description' => esc_html__('Items per row on screen resolution of device >=992px and < 1200px', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_md_slide_margin',
                [
                    'label' => esc_html__('Margin on Landscape Tablet', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 0,
                    'default' => 20,
                    'description' => esc_html__('Distance( or space) between 2 items on screen resolution of device >=992px and < 1200px', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel',
                        'owl_vertical' => 'false'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_sm_items',
                [
                    'label' => esc_html__('Items per row on Tablet', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 1,
                    'step' => 1,
                    'default' => 2,
                    'description' => esc_html__('Items per row on screen resolution of device >=768px and < 992px', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_sm_slide_margin',
                [
                    'label' => esc_html__('Margin on Tablet', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 0,
                    'default' => 20,
                    'description' => esc_html__('Distance( or space) between 2 items on screen resolution of device >=768px and < 992px', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel',
                        'owl_vertical' => 'false'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_xs_items',
                [
                    'label' => esc_html__('Items per row on Mobile Landscape', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 1,
                    'step' => 1,
                    'default' => 2,
                    'description' => esc_html__('Items per row on screen resolution of device >=480px and < 768px', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_xs_slide_margin',
                [
                    'label' => esc_html__('Margin on Mobile Landscape', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 0,
                    'default' => 20,
                    'description' => esc_html__('Distance( or space) between 2 items on screen resolution of device >=480px and < 768px', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel',
                        'owl_vertical' => 'false'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_ts_items',
                [
                    'label' => esc_html__('Items per row on Mobile', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 1,
                    'step' => 1,
                    'default' => 2,
                    'description' => esc_html__('Items per row on screen resolution of device < 480px', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'owl_ts_slide_margin',
                [
                    'label' => esc_html__('Margin on Mobile', 'furgan'),
                    'type' => \Elementor\Controls_Manager::NUMBER,
                    'min' => 0,
                    'default' => 10,
                    'description' => esc_html__('Distance( or space) between 2 items on screen resolution of device < 480px', 'furgan'),
                    'condition' => array(
                        'layout' => 'carousel',
                        'owl_vertical' => 'false'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'boostrap_rows_space',
                [
                    'label' => esc_html__('Rows space', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        'rows-space-0' => esc_html__('Default', 'furgan'),
                        'rows-space-10' => esc_html__('10px', 'furgan'),
                        'rows-space-20' => esc_html__('20px', 'furgan'),
                        'rows-space-30' => esc_html__('30px', 'furgan'),
                        'rows-space-40' => esc_html__('40px', 'furgan'),
                        'rows-space-50' => esc_html__('50px', 'furgan'),
                        'rows-space-60' => esc_html__('60px', 'furgan'),
                        'rows-space-70' => esc_html__('70px', 'furgan'),
                        'rows-space-80' => esc_html__('80px', 'furgan'),
                        'rows-space-90' => esc_html__('90px', 'furgan'),
                        'rows-space-100' => esc_html__('100px', 'furgan'),
                    ],
                    'default' => 'rows-space-0',
                    'condition' => array(
                        'layout' => 'grid'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'boostrap_bg_items',
                [
                    'label' => esc_html__('Items per row on Wide Screens', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        '12' => esc_html__('1 item', 'furgan'),
                        '6' => esc_html__('2 items', 'furgan'),
                        '4' => esc_html__('3 items', 'furgan'),
                        '3' => esc_html__('4 items', 'furgan'),
                        '15' => esc_html__('5 items', 'furgan'),
                        '2' => esc_html__('6 items', 'furgan'),
                    ],
                    'default' => '3',
                    'description' => esc_html__('Items per row on screen resolution of device >= 1500px', 'furgan'),
                    'condition' => array(
                        'layout' => 'grid'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'boostrap_lg_items',
                [
                    'label' => esc_html__('Items per row on Desktop', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        '12' => esc_html__('1 item', 'furgan'),
                        '6' => esc_html__('2 items', 'furgan'),
                        '4' => esc_html__('3 items', 'furgan'),
                        '3' => esc_html__('4 items', 'furgan'),
                        '15' => esc_html__('5 items', 'furgan'),
                        '2' => esc_html__('6 items', 'furgan'),
                    ],
                    'default' => '3',
                    'description' => esc_html__('Items per row on screen resolution of device >= 1200px and < 1500px', 'furgan'),
                    'condition' => array(
                        'layout' => 'grid'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'boostrap_md_items',
                [
                    'label' => esc_html__('Items per row on landscape Tablet', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        '12' => esc_html__('1 item', 'furgan'),
                        '6' => esc_html__('2 items', 'furgan'),
                        '4' => esc_html__('3 items', 'furgan'),
                        '3' => esc_html__('4 items', 'furgan'),
                        '15' => esc_html__('5 items', 'furgan'),
                        '2' => esc_html__('6 items', 'furgan'),
                    ],
                    'default' => '4',
                    'description' => esc_html__('Items per row on screen resolution of device >=992px and < 1200px', 'furgan'),
                    'condition' => array(
                        'layout' => 'grid'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'boostrap_sm_items',
                [
                    'label' => esc_html__('Items per row on portrait tablet', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        '12' => esc_html__('1 item', 'furgan'),
                        '6' => esc_html__('2 items', 'furgan'),
                        '4' => esc_html__('3 items', 'furgan'),
                        '3' => esc_html__('4 items', 'furgan'),
                        '15' => esc_html__('5 items', 'furgan'),
                        '2' => esc_html__('6 items', 'furgan'),
                    ],
                    'default' => '6',
                    'description' => esc_html__('Items per row on screen resolution of device >=768px and < 992px', 'furgan'),
                    'condition' => array(
                        'layout' => 'grid'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'boostrap_xs_items',
                [
                    'label' => esc_html__('Items per row on Mobile', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        '12' => esc_html__('1 item', 'furgan'),
                        '6' => esc_html__('2 items', 'furgan'),
                        '4' => esc_html__('3 items', 'furgan'),
                        '3' => esc_html__('4 items', 'furgan'),
                        '15' => esc_html__('5 items', 'furgan'),
                        '2' => esc_html__('6 items', 'furgan'),
                    ],
                    'default' => '6',
                    'description' => esc_html__('Items per row on screen resolution of device >=480  add < 768px', 'furgan'),
                    'condition' => array(
                        'layout' => 'grid'
                    ),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'boostrap_ts_items',
                [
                    'label' => esc_html__('Items per row on Mobile', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        '12' => esc_html__('1 item', 'furgan'),
                        '6' => esc_html__('2 items', 'furgan'),
                        '4' => esc_html__('3 items', 'furgan'),
                        '3' => esc_html__('4 items', 'furgan'),
                        '15' => esc_html__('5 items', 'furgan'),
                        '2' => esc_html__('6 items', 'furgan'),
                    ],
                    'default' => '6',
                    'description' => esc_html__('Items per row on screen resolution of device < 480px', 'furgan'),
                    'condition' => array(
                        'layout' => 'grid'
                    ),
                    'label_block' => true
                ]
            );
            $this->end_controls_section();

        }

        /**
         * Render the widget output on the frontend.
         *
         * Written in PHP and used to generate the final HTML.
         *
         * @since 1.0.0
         *
         * @access protected
         */
        protected function render()
        {
            $atts = $this->get_settings_for_display();
            $css_class = array('furgan-products');
            $css_class[] = $atts['style'];
            if ($atts['product_image_size']) {
                if ($atts['product_image_size'] == 'custom') {
                    $thumb_width = $atts['product_custom_thumb_width'];
                    $thumb_height = $atts['product_custom_thumb_height'];
                } else {
                    $product_image_size = explode("x", $atts['product_image_size']);
                    $thumb_width = $product_image_size[0];
                    $thumb_height = $product_image_size[1];
                }
                if ($thumb_width > 0) {
                    add_filter('furgan_shop_product_thumb_width', function () use ($thumb_width) {
                        return $thumb_width;
                    });
                }
                if ($thumb_height > 0) {
                    add_filter('furgan_shop_product_thumb_height', function () use ($thumb_height) {
                        return $thumb_height;
                    });
                }
            }
            $products = apply_filters('furgan_get_products', $atts);
            $total_product = $products->post_count;
            $list_class = array('response-product equal-container better-height');
            $item_class = array('product-item');
            $item_class[] = $atts['target'];
            $item_class[] = $atts['style'];
            $owl_settings = '';
            if ($atts['layout'] == 'grid') {
                $list_class[] = 'product-list-grid row auto-clear';
                $item_class[] = $atts['boostrap_rows_space'];
                $item_class[] = 'col-bg-' . $atts['boostrap_bg_items'];
                $item_class[] = 'col-lg-' . $atts['boostrap_lg_items'];
                $item_class[] = 'col-md-' . $atts['boostrap_md_items'];
                $item_class[] = 'col-sm-' . $atts['boostrap_sm_items'];
                $item_class[] = 'col-xs-' . $atts['boostrap_xs_items'];
                $item_class[] = 'col-ts-' . $atts['boostrap_ts_items'];
            }
            if ($atts['layout'] == 'carousel') {
                if ($total_product < $atts['owl_lg_items']) {
                    $atts['owl_loop'] = 'false';
                }
                $list_class[] = 'product-list-owl owl-slick';
                $item_class[] = $atts['owl_rows_space'];
                $owl_settings = apply_filters('furgan_carousel_data_attributes', 'owl_', $atts);
            }
            $id_loop = array();
            ?>
            <div class="<?php echo esc_attr(implode(' ', $css_class)); ?>">
                <?php if ($atts['title'] || $atts['desc']): ?>
                    <div class="furgan-title style-01">
                        <?php if ($atts['title']) : ?>
                            <h3 class="title">
                                <span><?php echo esc_html($atts['title']); ?></span>
                            </h3>
                        <?php endif; ?>
                        <?php if ($atts['desc']) : ?>
                            <div class="desc">
                                <?php echo wp_specialchars_decode($atts['desc']); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <?php if ($products->have_posts()): ?>
                    <div class="<?php echo esc_attr(implode(' ', $list_class)); ?>" <?php echo esc_attr($owl_settings); ?>>
                        <?php while ($products->have_posts()) : $products->the_post(); ?>
                            <?php $id_loop[] = get_the_ID(); ?>
                            <div <?php post_class($item_class); ?>>
                                <?php wc_get_template_part('product-styles/content-product', $atts['style']); ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php else: ?>
                    <p>
                        <strong><?php esc_html_e('No Product', 'furgan'); ?></strong>
                    </p>
                <?php endif; ?>
            </div>
            <?php wp_reset_postdata();
        }
    }
}