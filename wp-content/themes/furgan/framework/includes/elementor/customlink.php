<?php
if (!class_exists('Furgan_Elementor_Customlink')) {
    class Furgan_Elementor_Customlink extends Furgan_Elementor
    {
        public $name = 'customlink';
        public $title = 'Customlink';
        public $icon = 'furgan-elementor-icon eicon-anchor';

        /**
         * Register the widget controls.
         *
         * Adds different input fields to allow the user to change and customize the widget settings.
         *
         * @since 1.0.0
         *
         * @access protected
         */
        protected function register_controls()
        {

            $this->start_controls_section(
                'general_section',
                [
                    'label' => esc_html__('General', 'furgan'),
                ]
            );
            $this->add_control(
                'style',
                [
                    'label' => esc_html__('Style', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => Furgan_Elementor::furgan_elementor_preview($this->name),
                    'default' => 'style-01',
                    'label_block' => true
                ]
            );
            foreach (Furgan_Elementor::furgan_elementor_preview($this->name) as $key => $value) {
                $this->add_control(
                    $key,
                    [
                        'label' => '<img alt="' . esc_attr($this->name) . '" src="' . esc_url(get_theme_file_uri('/assets/images/elementor/' . $this->name . '/' . $key . '.jpg')) . '"/>',
                        'type' => \Elementor\Controls_Manager::HEADING,
                        'condition' => array(
                            'style' => $key
                        ),
                    ]
                );
            };
            $this->add_responsive_control(
                'align',
                [
                    'label' => esc_html__('Alignment', 'furgan'),
                    'type' => \Elementor\Controls_Manager::CHOOSE,
                    'options' => [
                        'left' => [
                            'title' => esc_html__('Left', 'furgan'),
                            'icon' => 'eicon-text-align-left',
                        ],
                        'center' => [
                            'title' => esc_html__('Center', 'furgan'),
                            'icon' => 'eicon-text-align-center',
                        ],
                        'right' => [
                            'title' => esc_html__('Right', 'furgan'),
                            'icon' => 'eicon-text-align-right',
                        ],
                        'justify' => [
                            'title' => esc_html__('Justified', 'furgan'),
                            'icon' => 'eicon-text-align-justify',
                        ],
                    ],
                    'selectors' => [
                        '{{WRAPPER}}' => 'text-align: {{VALUE}};',
                    ],
                ]
            );
            $this->add_control(
                'title', [
                    'label' => esc_html__('Title', 'furgan'),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'placeholder' => esc_html__('Enter your title', 'furgan'),
                    'condition' => array(
                        'style' => array('style-01', 'style-04', 'style-05', 'style-06')
                    ),
                    'label_block' => true
                ]
            );
            $repeater = new \Elementor\Repeater();
            $repeater->add_control(
                'type', [
                    'label' => esc_html__('Icon library', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => [
                        '' => esc_html__('None', 'furgan'),
                        'media' => esc_html__('Media', 'furgan'),
                        'icon' => esc_html__('Icon', 'furgan'),
                    ],
                    'default' => '',
                    'label_block' => true
                ]
            );
            $repeater->add_control(
                'media', [
                    'label' => esc_html__('Media', 'furgan'),
                    'type' => \Elementor\Controls_Manager::ICONS,
                    'fa4compatibility' => 'icon',
                    'default' => [
                        'value' => 'fas fa-star',
                        'library' => 'fa-solid',
                    ],
                    'condition' => array(
                        'type' => array('media')
                    ),
                    'label_block' => true
                ]
            );
            $repeater->add_control(
                'icon', [
                    'label' => esc_html__('Icon', 'furgan'),
                    'type' => \Elementor\Controls_Manager::ICON,
                    'options' => Furgan_Elementor::furgan_elementor_icon(),
                    'default' => 'fa fa-star',
                    'condition' => array(
                        'type' => array('icon')
                    ),
                    'label_block' => true
                ]
            );
            $repeater->add_control(
                'text', [
                    'label' => esc_html__('Text', 'furgan'),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'placeholder' => esc_html__('Enter your text', 'furgan'),
                    'label_block' => true
                ]
            );
            $repeater->add_control(
                'link', [
                    'label' => esc_html__('Link', 'furgan'),
                    'type' => \Elementor\Controls_Manager::URL,
                    'default' => [
                        'url' => '',
                        'is_external' => false,
                        'nofollow' => false,
                    ],
                    'label_block' => true
                ]
            );
            $this->add_control(
                'list',
                [
                    'label' => __('List Links', 'furgan'),
                    'type' => \Elementor\Controls_Manager::REPEATER,
                    'fields' => $repeater->get_controls(),
                    'title_field' => '{{{ text }}}',
                ]
            );
            $this->end_controls_section();

        }

        /**
         * Render the widget output on the frontend.
         *
         * Written in PHP and used to generate the final HTML.
         *
         * @since 1.0.0
         *
         * @access protected
         */
        protected function render()
        {
            $atts = $this->get_settings_for_display();
            $css_class = array('furgan-customlink furgan-custommenu');
            $css_class[] = $atts['style'];
            ?>
            <div class="<?php echo esc_attr(implode(' ', $css_class)); ?>">
                <?php if ($atts['title'] && in_array($atts['style'], array('style-01', 'style-04', 'style-05', 'style-06'))): ?>
                    <h2 class="widgettitle"><?php echo esc_html($atts['title']);?></h2>
                <?php endif; ?>
                <?php if ($atts['list']) { ?>
                    <ul class="menu">
                        <?php foreach ($atts['list'] as $list): ?>
                            <?php if ($list['text'] && $list['link']['url']):
                                $target = $list['link']['is_external'] ? '_blank' : '_self';?>
                                <li class="menu-item">
                                    <?php if ($list['type'] == 'icon' && $list['icon']): ?>
                                        <span class="icon">
                                            <span class="<?php echo esc_attr($list['icon']) ?>"></span>
                                        </span>
                                    <?php elseif ($list['type'] == 'media' && $list['media']): ?>
                                        <span class="icon">
                                            <?php Elementor\Icons_Manager::render_icon($list['media'], ['aria-hidden' => 'true']); ?>
                                        </span>
                                    <?php endif; ?>
                                    <a href="<?php echo esc_url($list['link']['url']) ?>"
                                       target="<?php echo esc_attr($target); ?>">
                                        <?php echo esc_html($list['text']); ?>
                                    </a>
                               </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                <?php } ?>
            </div>
            <?php
        }
    }
}