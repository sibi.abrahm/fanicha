<?php
if (!isset($content_width)) {
    $content_width = 900;
}
if (!class_exists('Furgan_Functions')) {
    class Furgan_Functions
    {
        /**
         * @var Furgan_Functions The one true Furgan_Functions
         * @since 1.0
         */
        private static $instance;

        public static function instance()
        {
            if (!isset(self::$instance) && !(self::$instance instanceof Furgan_Functions)) {
                self::$instance = new Furgan_Functions;
            }
            add_action('after_setup_theme', array(self::$instance, 'furgan_setup'));
            add_action('widgets_init', array(self::$instance, 'furgan_widgets_init'));
            add_action('wp_enqueue_scripts', array(self::$instance, 'furgan_enqueue_scripts'), 99);
            add_filter('get_default_comment_status', array(
                self::$instance,
                'furgan_open_default_comments_for_page'
            ), 10, 3);
            self::furgan_includes();

            return self::$instance;
        }

        public function furgan_setup()
        {
            load_theme_textdomain('furgan', get_template_directory() . '/languages');
            add_theme_support('automatic-feed-links');
            add_theme_support('title-tag');
            add_theme_support('post-thumbnails');
            add_theme_support('custom-background');
            add_theme_support('customize-selective-refresh-widgets');
            /*This theme uses wp_nav_menu() in two locations.*/
            register_nav_menus(array(
                    'primary' => esc_html__('Primary Menu', 'furgan'),
                    'vertical_menu' => esc_html__('Vertical Menu', 'furgan'),
                )
            );
            add_theme_support('html5',
                array(
                    'search-form',
                    'comment-form',
                    'comment-list',
                    'gallery',
                    'caption',
                )
            );
            add_theme_support('post-formats',
                array(
                    'image',
                    'video',
                    'quote',
                    'link',
                    'gallery',
                    'audio',
                )
            );
            /*Support WooCommerce*/
            add_theme_support('woocommerce');
            add_theme_support('wc-product-gallery-lightbox');
            add_theme_support('wc-product-gallery-slider');
            add_theme_support('wc-product-gallery-zoom');
        }

        /**
         * Register widget area.
         *
         * @since furgan 1.0
         *
         * @link  https://codex.wordpress.org/Function_Reference/register_sidebar
         */
        function furgan_widgets_init()
        {
            register_sidebar(
                array(
                    'name' => esc_html__('Widget Blog', 'furgan'),
                    'id' => 'widget-area',
                    'description' => esc_html__('Add widgets here to appear in your blog sidebar.', 'furgan'),
                    'before_widget' => '<div id="%1$s" class="widget %2$s">',
                    'after_widget' => '</div>',
                    'before_title' => '<h2 class="widgettitle">',
                    'after_title' => '<span class="arrow"></span></h2>',
                )
            );
            register_sidebar(array(
                    'name' => esc_html__('Widget Shop', 'furgan'),
                    'id' => 'widget-shop',
                    'description' => esc_html__('Add widgets here to appear in your shop sidebar.', 'furgan'),
                    'before_widget' => '<div id="%1$s" class="widget %2$s">',
                    'after_widget' => '</div>',
                    'before_title' => '<h2 class="widgettitle">',
                    'after_title' => '<span class="arrow"></span></h2>',
                )
            );
            register_sidebar(array(
                    'name' => esc_html__('Widget Product', 'furgan'),
                    'id' => 'widget-product',
                    'description' => esc_html__('Add widgets here to appear in your single product sidebar.', 'furgan'),
                    'before_widget' => '<div id="%1$s" class="widget %2$s">',
                    'after_widget' => '</div>',
                    'before_title' => '<h2 class="widgettitle">',
                    'after_title' => '<span class="arrow"></span></h2>',
                )
            );
        }

        /**
         * Register custom fonts.
         */
        function furgan_fonts_url()
        {
            /**
             * Translators: If there are characters in your language that are not
             * supported by Montserrat, translate this to 'off'. Do not translate
             * into your own language.
             */
            $furgan_enable_typography = $this->furgan_get_option('furgan_enable_typography');
            $furgan_typography_group = $this->furgan_get_option('typography_group');
            $settings = get_option('wpb_js_google_fonts_subsets');
            $font_families = array();
            if ($furgan_enable_typography == 1 && !empty($furgan_typography_group)) {
                foreach ($furgan_typography_group as $item) {
                    $font_families[] = str_replace(' ', '+', $item['furgan_typography_font_family']['family']);
                }
            }
            $font_families[] = 'Jost:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900';
            $font_families_str = '';
            $i = 0;
            foreach ($font_families as $font_family) {
                $i++;
                $pre_key = $i > 1 ? '&family=' : '';
                $font_families_str .= $pre_key . urlencode($font_family);
            }
            $query_args = array(
                'family' => $font_families_str,
            );
            if (!empty($settings)) {
                $query_args['subset'] = implode(',', $settings);
            }
            $fonts_url = add_query_arg($query_args, 'https://fonts.googleapis.com/css2');

            return esc_url_raw($fonts_url);
        }

        /**
         * Enqueue scripts and styles.
         *
         * @since furgan 1.0
         */
        function furgan_enqueue_scripts()
        {
            wp_dequeue_style('woocommerce_prettyPhoto_css');
            wp_dequeue_style('yith-wcwl-font-awesome');
            wp_dequeue_style('yith-quick-view');

            // Add custom fonts, used in the main stylesheet.
            wp_enqueue_style('furgan-fonts', self::furgan_fonts_url(), array(), null);
            /* Theme stylesheet. */
            wp_enqueue_style('animate-css');
            wp_enqueue_style('flaticon', get_theme_file_uri('/assets/fonts/flaticon/flaticon.css'), array(), '1.0');
            wp_enqueue_style('font-awesome', get_theme_file_uri('/assets/css/font-awesome.min.css'), array(), '1.0');
            wp_enqueue_style('bootstrap', get_theme_file_uri('/assets/css/bootstrap.min.css'), array(), '1.0');
            wp_enqueue_style('magnific-popup', get_theme_file_uri('/assets/css/magnific-popup.css'), array(), '1.0');
            wp_enqueue_style('slick', get_theme_file_uri('/assets/css/slick.min.css'), array(), '1.0');
            wp_enqueue_style('jquery-scrollbar', get_theme_file_uri('/assets/css/jquery.scrollbar.css'), array(), '1.0');
            wp_enqueue_style('chosen', get_theme_file_uri('/assets/css/chosen.min.css'), array(), '1.0');
            if (!class_exists('Furgan_Toolkit')) {
                wp_enqueue_style('mobile-menu', get_theme_file_uri('/assets/css/mobile-menu.css'), array(), '1.0');
            }
            wp_enqueue_style('furgan-style', get_theme_file_uri('/assets/css/style.css'), array(), '1.0', 'all');
            if (is_singular() && comments_open() && get_option('thread_comments')) {
                wp_enqueue_script('comment-reply');
            }
            /* SCRIPTS */
            if (!is_admin()) {
                wp_dequeue_style('woocommerce_admin_styles');
            }
            wp_enqueue_script('chosen', get_theme_file_uri('/assets/js/libs/chosen.min.js'), array(), '1.0', true);
            wp_enqueue_script('bootstrap', get_theme_file_uri('/assets/js/libs/bootstrap.min.js'), array(), '3.3.7', true);
            wp_enqueue_script('threesixty', get_theme_file_uri('/assets/js/libs/threesixty.min.js'), array(), '1.0.7', true);
            wp_enqueue_script('magnific-popup', get_theme_file_uri('/assets/js/libs/magnific-popup.min.js'), array(), '1.1.0', true);
            wp_enqueue_script('slick', get_theme_file_uri('/assets/js/libs/slick.min.js'), array(), '3.3.7', true);
            wp_enqueue_script('jquery-scrollbar', get_theme_file_uri('/assets/js/libs/jquery.scrollbar.min.js'), array(), '1.0.0', true);
            wp_enqueue_script('countdown', get_theme_file_uri('/assets/js/libs/countdown.min.js'), array(), '1.0.0', true);
            wp_enqueue_script('theia-sticky-sidebar', get_theme_file_uri('/assets/js/libs/theia-sticky-sidebar.min.js'), array(), '1.0.0', true);
            if (!class_exists('Furgan_Toolkit')) {
                wp_enqueue_script('mobile-menu', get_theme_file_uri('/assets/js/libs/mobile-menu.js'), array(), '1.0.0', true);
            }
            wp_enqueue_script('furgan-frontend', get_theme_file_uri('/assets/js/frontend.js'), array(), '1.0', true);
            wp_localize_script('furgan-frontend', 'furgan_ajax_frontend',
                array(
                    'ajaxurl' => admin_url('admin-ajax.php'),
                    'security' => wp_create_nonce('furgan_ajax_frontend'),
                    'added_to_cart_notification_text' => apply_filters('furgan_added_to_cart_notification_text', esc_html__('has been added to cart!', 'furgan')),
                    'view_cart_notification_text' => apply_filters('furgan_view_cart_notification_text', esc_html__('View Cart', 'furgan')),
                    'added_to_cart_text' => apply_filters('furgan_adding_to_cart_text', esc_html__('Product has been added to cart!', 'furgan')),
                    'wc_cart_url' => (function_exists('wc_get_cart_url') ? esc_url(wc_get_cart_url()) : ''),
                    'added_to_wishlist_text' => get_option('yith_wcwl_product_added_text', esc_html__('Product has been added to wishlist!', 'furgan')),
                    'wishlist_url' => (function_exists('YITH_WCWL') ? esc_url(YITH_WCWL()->get_wishlist_url()) : ''),
                    'browse_wishlist_text' => get_option('yith_wcwl_browse_wishlist_text', esc_html__('Browse Wishlist', 'furgan')),
                    'removed_cart_text' => esc_html__('Product Removed', 'furgan'),
                    'wp_nonce_url' => (function_exists('wc_get_cart_url') ? wp_nonce_url(wc_get_cart_url()) : ''),
                )
            );
            $furgan_enable_popup = $this->furgan_get_option('enable_popup');
            $furgan_enable_popup_mobile = $this->furgan_get_option('enable_popup_mobile');
            $furgan_popup_delay_time = $this->furgan_get_option('popup_delay_time');
            $atts = array(
                'owl_responsive_vertical' => 1199,
                'owl_loop' => false,
                'owl_slide_margin' => '14',
                'owl_focus_select' => true,
                'owl_ts_items' => $this->furgan_get_option('product_thumbnail_ts_items', 5),
                'owl_xs_items' => $this->furgan_get_option('product_thumbnail_xs_items', 5),
                'owl_sm_items' => $this->furgan_get_option('product_thumbnail_sm_items', 5),
                'owl_md_items' => $this->furgan_get_option('product_thumbnail_md_items', 5),
                'owl_lg_items' => $this->furgan_get_option('product_thumbnail_lg_items', 5),
                'owl_ls_items' => $this->furgan_get_option('product_thumbnail_bg_items', 5),
            );
            $atts = apply_filters('furgan_thumb_product_single_slide', $atts);
            $owl_settings = explode(' ', apply_filters('furgan_carousel_data_attributes', 'owl_', $atts));
            wp_localize_script('furgan-frontend', 'furgan_global_frontend',
                array(
                    'furgan_enable_popup' => $furgan_enable_popup,
                    'furgan_popup_delay_time' => $furgan_popup_delay_time,
                    'furgan_enable_popup_mobile' => $furgan_enable_popup_mobile,
                    'data_slick' => urldecode($owl_settings[3]),
                    'data_responsive' => urldecode($owl_settings[6]),
                    'countdown_day' => esc_html__('Days', 'furgan'),
                    'countdown_hrs' => esc_html__('Hours', 'furgan'),
                    'countdown_mins' => esc_html__('Mins', 'furgan'),
                    'countdown_secs' => esc_html__('Secs', 'furgan'),
                )
            );
        }

        public static function furgan_get_id()
        {
            $id_page = get_the_ID();
            if (class_exists('WooCommerce') && is_woocommerce() && !is_product()) {
                $id_page = get_option('woocommerce_shop_page_id');
                if (!$id_page) {
                    $id_page = get_the_ID();
                }
            }

            return $id_page;
        }

        public static function furgan_get_option($option_name, $default = '')
        {
            $get_value = isset($_GET[$option_name]) ? $_GET[$option_name] : '';
            $cs_option = null;
            if (defined('CS_VERSION')) {
                $cs_option = get_option(CS_OPTION);
            }
            if (isset($_GET[$option_name])) {
                $cs_option = $get_value;
                $default = $get_value;
            }
            $options = apply_filters('cs_get_option', $cs_option, $option_name, $default);
            if (!empty($option_name) && !empty($options[$option_name])) {
                $option = $options[$option_name];
                if (is_array($option) && isset($option['multilang']) && $option['multilang'] == true) {
                    if (defined('ICL_LANGUAGE_CODE')) {
                        if (isset($option[ICL_LANGUAGE_CODE])) {
                            return $option[ICL_LANGUAGE_CODE];
                        }
                    }
                }

                return $option;
            } else {
                return (!empty($default)) ? $default : null;
            }
        }

        /**
         * Filter whether comments are open for a given post type.
         *
         * @param string $status Default status for the given post type,
         *                             either 'open' or 'closed'.
         * @param string $post_type Post type. Default is `post`.
         * @param string $comment_type Type of comment. Default is `comment`.
         *
         * @return string (Maybe) filtered default status for the given post type.
         */
        function furgan_open_default_comments_for_page($status, $post_type, $comment_type)
        {
            if ('page' == $post_type) {
                return 'open';
            }

            return $status;
        }

        public static function furgan_includes()
        {
            include_once get_parent_theme_file_path('/framework/framework.php');
            defined('CS_ACTIVE_FRAMEWORK') or define('CS_ACTIVE_FRAMEWORK', true);
            defined('CS_ACTIVE_METABOX') or define('CS_ACTIVE_METABOX', true);
            defined('CS_ACTIVE_TAXONOMY') or define('CS_ACTIVE_TAXONOMY', false);
            defined('CS_ACTIVE_SHORTCODE') or define('CS_ACTIVE_SHORTCODE', false);
            defined('CS_ACTIVE_CUSTOMIZE') or define('CS_ACTIVE_CUSTOMIZE', false);
        }
    }
}
if (!function_exists('Furgan_Functions')) {
    function Furgan_Functions()
    {
        return Furgan_Functions::instance();
    }

    Furgan_Functions();
}