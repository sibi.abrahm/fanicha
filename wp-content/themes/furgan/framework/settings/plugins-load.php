<?php
if (!class_exists('Furgan_PluginLoad')) {
    class Furgan_PluginLoad
    {
        public $plugins = array();
        public $config = array();

        public function __construct()
        {
            $this->plugins();
            $this->config();
            if (!class_exists('TGM_Plugin_Activation')) {
                return;
            }
            if (function_exists('tgmpa')) {
                tgmpa($this->plugins, $this->config);
            }
        }

        public function plugins()
        {
            $this->plugins = array(
                array(
                    'name' => 'Furgan Toolkit',
                    'slug' => 'furgan-toolkit',
                    'source' => get_template_directory() . '/framework/plugins/furgan-toolkit.zip',
                    'version' => '1.0.1',
                    'required' => true,
                    'force_activation' => false,
                    'force_deactivation' => false,
                    'external_url' => '',
                    'image' => '',
                ),
                array(
                    'name' => 'Revolution Slider',
                    'slug' => 'revslider',
                    'source' => get_template_directory() . '/framework/plugins/revslider.zip',
                    'required' => true,
                    'version' => '',
                    'force_activation' => false,
                    'force_deactivation' => false,
                    'external_url' => '',
                    'image' => '',
                ),
                array(
                    'name' => 'Elementor Website Builder',
                    'slug' => 'elementor',
                    'required' => true,
                    'image' => '',
                ),
                array(
                    'name' => 'WooCommerce',
                    'slug' => 'woocommerce',
                    'required' => true,
                    'image' => '',
                ),
                array(
                    'name' => 'YITH WooCommerce Frequently Bought Together',
                    'slug' => 'yith-woocommerce-frequently-bought-together',
                    'required' => true,
                    'image' => '',
                ),
                array(
                    'name' => 'YITH WooCommerce Wishlist',
                    'slug' => 'yith-woocommerce-wishlist',
                    'required' => true,
                    'image' => '',
                ),
                array(
                    'name' => 'YITH WooCommerce Quick View',
                    'slug' => 'yith-woocommerce-quick-view',
                    'required' => true,
                    'image' => '',
                ),
                array(
                    'name' => 'Variation Swatches for WooCommerce',
                    'slug' => 'woo-variation-swatches',
                    'required' => true,
                    'image' => '',
                ),
                array(
                    'name' => 'Contact Form 7',
                    'slug' => 'contact-form-7',
                    'required' => true,
                    'image' => '',
                ),
                array(
                    'name' => 'MC4WP: Mailchimp for WordPress',
                    'slug' => 'mailchimp-for-wp',
                    'required' => true,
                    'image' => '',
                ),
                array(
                    'name' => 'Classic Editor',
                    'slug' => 'classic-editor',
                    'required' => true,
                    'image' => '',
                ),
                array(
                    'name' => 'Classic Widgets',
                    'slug' => 'classic-widgets',
                    'required' => true,
                    'image' => '',
                ),
                array(
                    'name' => 'WordPress Importer',
                    'slug' => 'wordpress-importer',
                    'required' => true,
                    'image' => '',
                )
            );
        }

        public function config()
        {
            $this->config = array(
                'id' => 'furgan',
                'default_path' => '',
                'menu' => 'furgan-install-plugins',
                'parent_slug' => 'themes.php',
                'capability' => 'edit_theme_options',
                'has_notices' => true,
                'dismissable' => true,
                'dismiss_msg' => '',
                'is_automatic' => true,
                'message' => '',
            );
        }
    }
}
if (!function_exists('Furgan_PluginLoad')) {
    function Furgan_PluginLoad()
    {
        new  Furgan_PluginLoad();
    }
}
add_action('tgmpa_register', 'Furgan_PluginLoad');