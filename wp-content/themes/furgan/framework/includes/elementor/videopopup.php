<?php
if (!class_exists('Furgan_Elementor_Videopopup')) {
    class Furgan_Elementor_Videopopup extends Furgan_Elementor
    {
        public $name = 'videopopup';
        public $title = 'Video Popup';
        public $icon = 'furgan-elementor-icon eicon-video-playlist';

        /**
         * Register the widget controls.
         *
         * Adds different input fields to allow the user to change and customize the widget settings.
         *
         * @since 1.0.0
         *
         * @access protected
         */
        protected function register_controls()
        {

            $this->start_controls_section(
                'general_section',
                [
                    'label' => esc_html__('General', 'furgan'),
                ]
            );
            $this->add_control(
                'style',
                [
                    'label' => esc_html__('Style', 'furgan'),
                    'type' => \Elementor\Controls_Manager::SELECT,
                    'options' => Furgan_Elementor::furgan_elementor_preview($this->name),
                    'default' => 'style-01',
                    'label_block' => true
                ]
            );
            foreach (Furgan_Elementor::furgan_elementor_preview($this->name) as $key => $value) {
                $this->add_control(
                    $key,
                    [
                        'label' => '<img alt="' . esc_attr($this->name) . '" src="' . esc_url(get_theme_file_uri('/assets/images/elementor/' . $this->name . '/' . $key . '.jpg')) . '"/>',
                        'type' => \Elementor\Controls_Manager::HEADING,
                        'condition' => array(
                            'style' => $key
                        ),
                    ]
                );
            }
            $this->add_control(
                'image', [
                    'label' => esc_html__('Image', 'furgan'),
                    'type' => \Elementor\Controls_Manager::MEDIA,
                    'default' => [
                        'url' => \Elementor\Utils::get_placeholder_image_src(),
                    ],
                    'label_block' => true
                ]
            );
            $this->add_control(
                'cate', [
                    'label' => esc_html__('Label', 'furgan'),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'placeholder' => esc_html__('Enter your label', 'furgan'),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'title', [
                    'label' => esc_html__('Title', 'furgan'),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'placeholder' => esc_html__('Enter your title', 'furgan'),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'desc', [
                    'label' => esc_html__('Description', 'furgan'),
                    'type' => \Elementor\Controls_Manager::WYSIWYG,
                    'placeholder' => esc_html__('Enter your description', 'furgan'),
                    'label_block' => true
                ]
            );
            $this->add_control(
                'link', [
                    'label' => esc_html__('Link', 'furgan'),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'placeholder' => esc_html__('https://your-link.com', 'furgan'),
                    'label_block' => true
                ]
            );
            $this->end_controls_section();

        }

        /**
         * Render the widget output on the frontend.
         *
         * Written in PHP and used to generate the final HTML.
         *
         * @since 1.0.0
         *
         * @access protected
         */
        protected function render()
        {
            $atts = $this->get_settings_for_display();
            $css_class = array('furgan-videopopup');
            $css_class[] = $atts['style'];
            ?>
            <div class="<?php echo esc_attr(implode(' ', $css_class)); ?>">
                <div class="videopopup-inner">
                    <?php if ($atts['image']): ?>
                        <figure class="videopopup-thumb">
                            <?php echo wp_get_attachment_image($atts['image']['id'], 'full'); ?>
                            <?php if ($atts['link']):?>
                                <div class="product-video-button">
                                    <a href="<?php echo esc_url($atts['link']); ?>"></a>
                                </div>
                            <?php endif; ?>
                        </figure>
                    <?php endif; ?>
                    <div class="videopopup-info">
                        <?php if ($atts['cate']): ?>
                            <div class="cate"><?php echo esc_html($atts['cate']); ?></div>
                        <?php endif; ?>
                        <?php if ($atts['title']): ?>
                            <h3 class="title">
                                <?php echo esc_html($atts['title']); ?>
                            </h3>
                        <?php endif; ?>
                        <?php if ($atts['desc']): ?>
                            <div class="desc"><?php echo wp_specialchars_decode($atts['desc']); ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php
        }
    }
}