<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( ! class_exists( 'Furgan_Pinmap_Builder' ) ) {
	class  Furgan_Pinmap_Builder
	{
		/**
		 * @var Furgan_Pinmap_Builder The one true Furgan_Pinmap_Builder
		 */
		private static $instance;

		public static function instance()
		{
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Furgan_Pinmap_Builder ) ) {
				self::$instance = new Furgan_Pinmap_Builder;
				self::$instance->includes();
				/* Add image size for woocommerce product */
				$size_thumb = apply_filters( 'furgan_pinmap_product_thumbnail', array(
						'width'  => 100,
						'height' => 150,
						'crop'   => true
					)
				);
				add_image_size( 'furgan-pinmap-thumbnail', $size_thumb['width'], $size_thumb['height'], $size_thumb['crop'] );
			}

			return self::$instance;
		}

		public function includes()
		{
			require_once FURGAN_TOOLKIT_PATH . 'includes/mapper/includes/post-type.php';
			require_once FURGAN_TOOLKIT_PATH . 'includes/mapper/includes/shortcode.php';
		}
	}
}
if ( ! function_exists( 'Furgan_Pinmap_Builder' ) ) {
	function Furgan_Pinmap_Builder()
	{
		return Furgan_Pinmap_Builder::instance();
	}
}
Furgan_Pinmap_Builder();