<?php
if (!defined('ABSPATH')) {
    exit;
}
/**
 * Furgan Mailchimp
 *
 * Displays Mailchimp widget.
 *
 * @category Widgets
 * @package  Furgan/Widgets
 * @version  1.0.0
 * @extends  FURGAN_Widget
 */
if (!class_exists('Furgan_Mailchimp_Widget')) {
    class Furgan_Mailchimp_Widget extends FURGAN_Widget
    {
        /**
         * Constructor.
         */
        public function __construct()
        {
            $array_settings = apply_filters('furgan_filter_settings_widget_mailchimp',
                array(
                    'title' => array(
                        'type' => 'text',
                        'title' => esc_html__('Title', 'furgan-toolkit'),
                        'default' => esc_html__('Newsletter', 'furgan-toolkit'),
                    ),
                    'description' => array(
                        'type' => 'text',
                        'title' => esc_html__('Description:', 'furgan-toolkit'),
                        'default' => esc_html__('To stay up-to-date on our promotions, discounts, sales and more', 'furgan-toolkit'),
                    ),
                )
            );
            $this->widget_cssclass = 'widget-furgan-mailchimp';
            $this->widget_description = esc_html__('Display the customer Newsletter.', 'furgan-toolkit');
            $this->widget_id = 'widget_furgan_mailchimp';
            $this->widget_name = esc_html__('Furgan: Newsletter', 'furgan-toolkit');
            $this->settings = $array_settings;
            parent::__construct();
        }

        /**
         * Output widget.
         *
         * @see WP_Widget
         *
         * @param array $args
         * @param array $instance
         */
        public function widget($args, $instance)
        {
            $this->widget_start($args, $instance);
            ob_start();
            ?>
            <div class="newsletter-form-wrap">
                <div class="desc"><?php echo esc_html($instance['description']); ?></div>
                <div class="form-newsletter">
                    <?php
                    if (function_exists('mc4wp_show_form')) {
                        $mc4wparr  = array(
                            'posts_per_page' => 1,
                            'post_type'      => 'mc4wp-form',
                            'post_status'    => 'publish',
                            'fields'         => 'ids',
                        );
                        $mc4wp = get_posts($mc4wparr);
                        if ($mc4wp) {
                            foreach ($mc4wp as $post_id) {
                                mc4wp_show_form(intval($post_id));
                            }
                        }
                    }
                    ?>
                </div>
            </div>
            <?php
            echo apply_filters('furgan_filter_widget_newsletter', ob_get_clean(), $instance);
            $this->widget_end($args);
        }
    }
}
add_action('widgets_init', 'Furgan_Mailchimp_Widget');
if (!function_exists('Furgan_Mailchimp_Widget')) {
    function Furgan_Mailchimp_Widget()
    {
        register_widget('Furgan_Mailchimp_Widget');
    }
}