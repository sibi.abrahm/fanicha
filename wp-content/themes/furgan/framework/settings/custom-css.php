<?php
if (!function_exists('furgan_custom_inline_css')) {
    function furgan_custom_inline_css()
    {
        $css = furgan_theme_color();
        $content = preg_replace('/\s+/', ' ', $css);
        wp_add_inline_style('furgan-style', $content);
    }
}
add_action('wp_enqueue_scripts', 'furgan_custom_inline_css', 999);
if (!function_exists('furgan_theme_color')) {
    function furgan_theme_color()
    {
        $main_color = Furgan_Functions::furgan_get_option('main_color', '#9ab968');
        $enable_typography = Furgan_Functions::furgan_get_option('enable_typography');
        $typography_group = Furgan_Functions::furgan_get_option('typography_group');
        $width = Furgan_Functions::furgan_get_option('width_logo', '170');
        $width .= 'px';
        $css = '
        header .logo {
            width: ' . $width . ';
        }
        ';
        if ($enable_typography == 1 && !empty($furgan_typography_group)) {
            foreach ($typography_group as $item) {
                $css .= '
					' . $item['element_tag'] . '{
						font-family: ' . $item['typography_font_family']['family'] . ';
						font-weight: ' . $item['typography_font_family']['variant'] . ';
						font-size: ' . $item['typography_font_size'] . 'px;
						line-height: ' . $item['typography_line_height'] . 'px;
						color: ' . $item['body_text_color'] . ';
					}
				';
            }
        }
        $css .= '
		.page-links > span:not(.page-links-title),
        .page-links > a:hover,
        .furgan-banner.style-05 .button:hover {
            background-color: ' . $main_color . ';
            border-color: ' . $main_color . ';
        }
        .post-password-form input[type="submit"]:hover,
        .woocommerce-error .button, .woocommerce-info .button, .woocommerce-message .button,
        .widget_shopping_cart .woocommerce-mini-cart__buttons .button:hover,
        .widget_shopping_cart .woocommerce-mini-cart__buttons .button.checkout,
        #widget-area .widget .select2-container--default .select2-selection--multiple .select2-selection__choice,
        .furgan-products.style-03 .owl-slick .slick-arrow:hover,
        .slick-dots li button:hover,
        .slick-dots li.slick-active button,
        .block-menu-bar .menu-bar:hover span,
        .furgan-live-search-form .keyword-current,
        .block-search-form .btn-submit,
        .chosen-results > .scroll-element .scroll-bar:hover,
        .meta-woo .block-woo .block-link .count,
        .block-user #wp-submit,
        .block-minicart .cart_list > .scroll-element .scroll-bar:hover,
        .vertical-wrapper.block-nav-category .block-title,
        .header.style-01 .header-top,
        .header.style-02 .header-top,
        a.backtotop,
        .post-item .slick-arrow:hover,
        .datebox,
        .comment-form .form-submit #submit:hover,
        .widget_product_search .woocommerce-product-search button[type="submit"]:hover,
        .widget_search .search-form button:hover,
        #widget-area .widget .select2-container--default .select2-selection--multiple .select2-selection__choice,
        .woocommerce-widget-layered-nav-dropdown .woocommerce-widget-layered-nav-dropdown__submit,
        .widget_price_filter .button,
        .widget_price_filter .ui-slider-range,
        .widget_price_filter .ui-slider-range::before,
        .woocommerce-pagination span.page-numbers.current,
        .woocommerce-pagination span.page-numbers:hover,
        .woocommerce-pagination a.page-numbers:hover,
        .woocommerce-pagination a.page-numbers:hover,
        .woocommerce-pagination li .page-numbers.current,
        .woocommerce-pagination li .page-numbers:hover,
        .comments-pagination .page-numbers.current,
        .comments-pagination a.page-numbers:hover,
        .post-pagination > span:not(.title),
        .post-pagination a span:hover,
        .pagination .page-numbers.current,
        .pagination .page-numbers:hover,
        #yith-quick-view-close,
        .onsale,
        .woocommerce-order .woocommerce-customer-details .woocommerce-column__title,
        body.woocommerce-account .woocommerce-notices-wrapper ~ h2::before,
        form.woocommerce-form-login .button,
        form.register .button,
        .woocommerce-MyAccount-content fieldset ~ p .woocommerce-Button:hover,
        .woocommerce-password-strength.strong::after,
        .woocommerce-ResetPassword .form-row .woocommerce-Button:hover,
        .woocommerce table.wishlist_table td.product-add-to-cart a:hover,
        .track_order .button:hover,
        .main-container.error-404 .search-form .search-submit,
        #popup-newsletter .newsletter-form-wrap .submit-newsletter:hover,
        .furgan-banner.style-11 .button,
        .furgan-title.style-01 .title::before,
        .furgan-title.style-01 .title::after,
        .furgan-title.style-02 .title::before,
        .wpcf7-form .wpcf7-submit,
        .furgan-team .social-list a:hover,
        .furgan-newsletter.style-01 input[type="submit"]:hover,
        .furgan-newsletter.style-03 input[type="submit"]:hover,
        .furgan-socials.style-01 .socials-list li a:hover,
        .furgan-socials.style-02 .socials-list li a:hover,
        .furgan-socials.style-03 .socials-list li a:hover,
        .furgan-socials.style-05 .socials-list li a:hover,
        .furgan-verticalmenu.block-nav-category .block-title,
        .furgan-custommenu .menu .menu-item .icon,
        .product-item.list .add-to-cart a:hover {
            background-color: ' . $main_color . ';
        }
        .mc4wp-response .mc4wp-alert.mc4wp-success,
        .comment-form .comment-form-cookies-consent #wp-comment-cookies-consent:checked + label::before,
        article.sticky .post-title a::before,
        .widget_shopping_cart .woocommerce-mini-cart__total .woocommerce-Price-amount,
        a:hover, a:focus, a:active,
        .owl-slick .slick-arrow:hover,
        .furgan-products.style-06 .owl-slick .slick-arrow:hover,
         .box-header-nav .main-menu .menu-item:hover > a,
        .box-header-nav .main-menu .menu-item .sub-menu .menu-item:hover > a,
        .box-header-nav .main-menu .menu-item:hover > .toggle-submenu,
        .box-header-nav .main-menu > .menu-item.menu-item-right:hover > a,
        .furgan-live-search-form .product-price ins,
        .vertical-menu .menu-item.parent:hover > a::after,
        .vertical-menu > .menu-item:hover > a,
        .vertical-menu > .menu-item.show-submenu > a,
        .vertical-menu > .menu-item.parent:hover > a::after,
        .vertical-menu > .menu-item.show-submenu > a::after,
        .block-nav-category .view-all-category a,
        .post-item .post-author::before,
        .post-item .date a::before,
        .post-item .post-comment a::before,
        .post-item .post-comment-icon a::before,
        .woocommerce-widget-layered-nav-list li.chosen a,
        .widget_categories .cat-item.current-cat > a,
        .widget_pages .page_item.current_page_item > a,
        .widget_product_categories .cat-item.current-cat > a,
        .grid-view-mode .modes-mode:hover,
        .grid-view-mode .modes-mode:focus,
        .grid-view-mode .modes-mode:active,
        .grid-view-mode .modes-mode.active ,
        .woocommerce-MyAccount-navigation > ul li.is-active a,
        .woocommerce table.wishlist_table tr td.product-stock-status span.wishlist-in-stock,
        .wishlist_table.mobile li table.additional-info td.value .wishlist-in-stock,
        #popup-newsletter button.close:hover,
        .furgan-banner.style-02 .banner-label,
        .furgan-banner.style-03 .title a,
        .furgan-banner.style-05 .subtitle,
        .furgan-banner.style-06 .title,
        .furgan-banner.style-07 .title,
        .furgan-banner.style-08 .subtitle,
        .furgan-banner.style-09 .title,
        .furgan-banner.style-10 .title,
        .furgan-banner.style-10 .button,
        .furgan-banner.style-11 .button:hover,
        .furgan-title .icon,
        .furgan-iconbox.style-03 .icon,
        .furgan-iconbox.style-04 .icon,
        .furgan-newsletter.style-02 input[type="submit"]:hover,
        .furgan-videopopup .cate,
        .furgan-testimonial.style-02 .desc::before,
        .furgan-testimonial.style-03 .desc::before,
        .datebox span:first-child,
        .header-top-inner .top-bar-menu.right > .menu-item a {
            color: ' . $main_color . ';
        }
        .woocommerce-error, .woocommerce-info, .woocommerce-message {
            border-top: 3px solid ' . $main_color . ';
        }
        .widget_product_tag_cloud .tagcloud a:hover,
        .widget_tag_cloud .tagcloud a:hover {
            border-color: ' . $main_color . ';
            color: ' . $main_color . ';
        }
        .furgan-live-search-form.loading .search-box::before {
            border-top-color: ' . $main_color . ';
        }
        .furgan-banner.style-04 .title::before,
        .post-grid .post-title::before {
            border-bottom: 2px solid ' . $main_color . ';
        }
        .widget-furgan-socials .socials-list li a::before,
        #widget-area .widget_nav_menu li a::before,
        .widget_archive li a::before,
        .widget_recent_entries li a::before,
        .woocommerce-widget-layered-nav-list li a::before,
        .widget_categories .cat-item > a::before,
        .widget_pages .page_item > a::before,
        .widget_product_categories .cat-item > a::before {
            border-color: transparent transparent transparent ' . $main_color . ';
        } 
        .widget_price_filter .ui-slider-handle {
            border-color: ' . $main_color . ' transparent transparent transparent;
        }
        .onsale::before {
            border-color: ' . $main_color . ' transparent ' . $main_color . ' transparent;
        }
        .furgan-banner.style-03 .title {
            border-bottom: 1px solid ' . $main_color . ';
        }
        .furgan-iconbox.style-01 .iconbox-inner:hover,
        .furgan-iconbox.style-03 .iconbox-inner:hover,
        .furgan-tabs.style-01 .tab-link li a:hover::before,
        .furgan-tabs.style-01 .tab-link li.active a::before {
            border-color: ' . $main_color . ';
        }
        .furgan-instagram.style-02 .furgan-title.style-01 .title::before,
        .furgan-custommenu.style-01 .widgettitle::before {
            border-left: 2px solid ' . $main_color . ';
        }
        .furgan-testimonial.style-01 .name::before,
        .furgan-custommenu.style-05 .widgettitle::before,
        .furgan-newsletter.style-04 .title::before {
            border-bottom: 2px solid ' . $main_color . ';
        }
        .furgan-newsletter.style-04 input[type="submit"],
        .furgan-newsletter.style-05 input[type="submit"] {
            background: ' . $main_color . ';
        }
        .furgan-videopopup .videopopup-thumb .product-video-button a::after {
            border-color: transparent transparent transparent ' . $main_color . ';
        }
        .furgan-products.style-06 .furgan-title.style-01 .title::before {
            border-left: 2px solid ' . $main_color . ';
        }
        .furgan-tabs.style-01 .tab-head .tab-link li a::before {
            border-bottom: 15px solid ' . $main_color . ';
        }
        .furgan-testimonial.style-03 .thumb-avatar img {
            border: 3px solid ' . $main_color . ';
        }
        .furgan-testimonial.style-01 .thumb-avatar img,
        .post-single .categories,
        .furgan-verticalmenu.block-nav-category .block-content {
            border: 2px solid ' . $main_color . ';
        }
        .page-title::before {
            border-bottom: 4px solid ' . $main_color . ';
        }
		';
        return apply_filters('furgan_main_custom_css', $css);
    }
}
