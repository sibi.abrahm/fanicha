<?php
if (!defined('ABSPATH')) {
    exit; // disable direct access
}
require_once(ABSPATH . 'wp-admin/includes/media.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
require_once(ABSPATH . 'wp-admin/includes/image.php');

require_once(ABSPATH . 'wp-admin/includes/misc.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');
if (!class_exists('FURGAN_IMPORTER')) {
    class FURGAN_IMPORTER
    {
        public $data_demos = array();
        public $widget_path;
        public $revslider_path;
        public $item_import;

        public function __construct()
        {
            $this->define_constants();
            $menu_location = array(
                'primary' => 'Primary Menu',
                'vertical_menu' => 'Vertical Menu',
                'furgan_mobile_menu' => 'Primary Menu',
            );
            $megamenu = array(
                'enable_mega' => 1,
                'hide_title' => 0,
                'disable_link' => 0,
                'menu_width' => '',
                'menu_icon' => '',
                'menu_content_id' => 1,
                'menu_bg' => 0,
                'menu_content' => '',
                'mega_responsive' => '',
                'icon_image' => '',
                'label_image' => '',
                'bg_position' => 'center',
                'menu_icon_type' => 'font-icon',
            );
            $custom_metabox = array(
                'home-01' => array(
                    'enable_header' => 1,
                    'metabox_header_options' => 'style-02',
                    'enable_banner' => 1,
                    'metabox_banner_type' => 'disable',
                ),
                'home-02' => array(
                    'enable_banner' => 1,
                    'metabox_banner_type' => 'disable',
                    'enable_footer' => 1,
                    'metabox_footer_options' => 'footer-02'
                ),
                'home-03' => array(
                    'enable_header' => 1,
                    'metabox_header_options' => 'style-03',
                    'metabox_enable_header_transparent' => 1,
                    'enable_banner' => 1,
                    'metabox_banner_type' => 'disable',
                    'enable_footer' => 1,
                    'metabox_footer_options' => 'footer-03'
                ),
                'home-04' => array(
                    'enable_header' => 1,
                    'metabox_header_options' => 'style-04',
                    'metabox_enable_header_transparent' => 1,
                    'enable_banner' => 1,
                    'metabox_banner_type' => 'disable',
                    'enable_footer' => 1,
                    'metabox_footer_options' => 'footer-04'
                ),
                'home-05' => array(
                    'enable_header' => 1,
                    'metabox_header_options' => 'style-05',
                    'metabox_enable_header_transparent' => 1,
                    'enable_banner' => 1,
                    'metabox_banner_type' => 'disable',
                    'enable_footer' => 1,
                    'metabox_footer_options' => 'footer-05'
                ),
                'contact' => array(
                    'enable_banner' => 1,
                    'metabox_banner_type' => 'disable'
                ),
            );
            $default_template = array(
                'about',
                'contact',
                'cart',
                'checkout',
                'compare',
                'my-account',
                'order-tracking',
                'wishlist',
                'banner',
                'blog-element',
                'product-element',
                'icon-box',
                'team',
                'instagram-feed',
                'testimonials',
                'newsletter'
            );
            $delete_post = array(
                'hello-world',
                'cart-2',
                'checkout-2',
                'my-account-2',
                'privacy-policy',
                'refund_returns',
                'sample-page',
                'shop-2',
                'wishlist-2'
            );
            $compare = array(
                'compare_page' => 1,
                'show_in_products_list' => 'yes',
                'products_loop_hook' => 'dreaming_wccp_shop_loop',
                'show_in_single_product' => 'yes',
                'single_product_hook' => 'woocommerce_after_add_to_cart_form',
                'show_compare_panel' => 'yes',
                'panel_img_size_w' => 150,
                'panel_img_size_h' => 150,
                'compare_img_size_w' => 180,
                'compare_img_size_h' => 200,
                'compare_fields_attrs' => 'image,add-to-cart,title,price,sku,stock',
                'all_compare_fields_attrs_order' => 'image,add-to-cart,title,price,sku,stock,weight,dimensions,description,pa_color'
            );
            $settings_options = array(
                'home' => 'home-01',
                'blog' => 'blog',
                'blogname' => 'Furgan',
                'blogdescription' => 'Furniture Elementor WooCommerce Theme'
            );
            $data_filter = array(
                'data_demos' => array(
                    array(
                        'name' => esc_html__('Import Demo', 'furgan-toolkit'),
                        'preview' => get_theme_file_uri('screenshot.jpg'),
                        'demo_link' => '',
                        'theme_option' => FURGAN_IMPORTER_DIR . '/data/theme-options.txt',
                        'widget_path' => FURGAN_IMPORTER_DIR . '/data/widgets.wie',
                        'revslider_path' => FURGAN_IMPORTER_DIR . '/data/revsliders/',
                        'menu_locations' => $menu_location,
                        'megamenu' => $megamenu,
                        'custom_metabox' => $custom_metabox,
                        'default_template' => $default_template,
                        'delete_post' => $delete_post,
                        'compare' => $compare,
                        'settings_options' => $settings_options,
                    ),
                ),
                'item_import' => array(
                    'kt_import_theme_options' => 'Import Theme Options',
                    'kt_import_widget' => 'Import Widget',
                    'kt_import_revslider' => 'Import Revslider',
                    'kt_import_config' => 'Import Config',
                ),
            );
            $import_data = apply_filters('furgan_data_import', $data_filter);
            // SET DATA DEMOS
            $this->data_demos = isset($import_data['data_demos']) ? $import_data['data_demos'] : array();
            $this->item_import = isset($import_data['item_import']) ? $import_data['item_import'] : array();
            // JS and css
            add_action('admin_enqueue_scripts', array($this, 'register_scripts'));
            add_action('importer_page_content', array($this, 'importer_page_content'));
            /* Register ajax action */
            add_action('wp_ajax_kt_import_theme_options', array($this, 'import_theme_options'));
            add_action('wp_ajax_kt_import_widget', array($this, 'import_widget'));
            add_action('wp_ajax_kt_import_revslider', array($this, 'import_revslider'));
            add_action('wp_ajax_kt_import_config', array($this, 'import_config'));
        }

        /**
         * Define  Constants.
         */
        public function define_constants()
        {
            $this->define('FURGAN_IMPORTER_DIR', plugin_dir_path(__FILE__));
            $this->define('FURGAN_IMPORTER_URI', plugin_dir_url(__FILE__));
        }

        /**
         * Define constant if not already set.
         *
         * @param  string $name
         * @param  string|bool $value
         */
        public function define($name, $value)
        {
            if (!defined($name)) {
                define($name, $value);
            }
        }

        public function register_scripts($hook_suffix)
        {
            if ($hook_suffix == 'toplevel_page_furgan_menu') {
                wp_enqueue_style('thickbox');
                wp_enqueue_style('kt-importer-style', FURGAN_IMPORTER_URI . '/assets/circle.css');
                wp_enqueue_style('kt-importer-circle', FURGAN_IMPORTER_URI . '/assets/import.css');
                wp_enqueue_script('thickbox');
                wp_enqueue_script('kt-importer-script', FURGAN_IMPORTER_URI . '/assets/import.js', array('jquery'), false);
            }
        }

        public function importer_page_content()
        {
            $theme_name = wp_get_theme()->get('Name');
            ?>
            <div class="kt-importer-wrapper">
                <div class="progress_test" style="height: 5px; background-color: red; width: 0;"></div>
                <h1 class="heading"><?php echo ucfirst(esc_html($theme_name)); ?>
                    - <?php esc_html_e('Install Demo Content', 'furgan-toolkit'); ?></h1>
                <div class="note">
                    <h3><?php esc_html_e('Please read before importing:', 'furgan-toolkit'); ?></h3>
                    <p><?php esc_html_e('This importer will help you build your site look like our demo. Importing data is recommended on fresh install.', 'furgan-toolkit'); ?></p>
                    <p><?php esc_html_e('Please ensure you have already installed and activated', 'furgan-toolkit'); ?>
                        <strong>
                            <?php esc_html_e('Furgan Toolkit, WooCommerce, Elementor Website Builder and Revolution Slider plugins.', 'furgan-toolkit'); ?></strong>
                    </p>
                    <p><?php esc_html_e('Please note that importing data only builds a frame for your website.', 'furgan-toolkit'); ?>
                        <strong>
                            <?php esc_html_e('It will import all demo contents.', 'furgan-toolkit'); ?></strong></p>
                    <p><?php esc_html_e('It can take a few minutes to complete.', 'furgan-toolkit'); ?> <strong>
                            <?php esc_html_e('Please don\'t close your browser whileimporting.', 'furgan-toolkit'); ?></strong>
                    </p>
                </div>
                <?php if (!empty($this->data_demos)) : ?>
                    <div class="options theme-browser">
                        <?php foreach ($this->data_demos as $key => $data): ?>
                            <div id="option-<?php echo $key; ?>" class="option">
                                <div class="inner">
                                    <div class="preview">
                                        <img src="<?php echo $data['preview']; ?>">
                                    </div>
                                    <span
                                        class="more-details"><?php esc_html_e('HAVE IMPORTED', 'furgan-toolkit'); ?></span>
                                    <h3 class="demo-name theme-name"><?php echo $data['name']; ?></h3>
                                    <div class="group-control theme-actions">
                                        <div class="control-inner">
                                            <button data-id="<?php echo $key; ?>"
                                                    data-optionid="<?php echo $key; ?>"
                                                    class="button button-primary open-import"><?php esc_html_e('Install', 'furgan-toolkit'); ?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div id="content-demo-<?php echo $key; ?>" class="option" style="display: none;">
                                    <div class="inner" data-option="<?php echo $key; ?>">
                                        <div class="plugin-check">
                                            <strong><?php esc_html_e('The Following Required To Import Content !', 'furgan-toolkit'); ?></strong>
                                            <p>
                                                <span><?php esc_html_e('PHP Version > 5.6, max_execution_time 180', 'furgan-toolkit'); ?></span>
                                                <span>( * )</span>
                                            </p>
                                            <p>
                                                <span><?php esc_html_e('Your Host allow download file from other site and zip file', 'furgan-toolkit'); ?></span>
                                                <span>( * )</span>
                                            </p>
                                            <p>
                                                <span><?php esc_html_e('memory_limit 128M, post_max_size 32M, upload_max_filesize 32M', 'furgan-toolkit'); ?></span>
                                                <span>( * )</span>
                                            </p>
                                        </div>
                                        <div class="block-title">
                                            <h3 class="demo-name"><?php echo $data['name']; ?></h3>
                                        </div>
                                        <div class="kt-control">
                                            <h4 class="import-title"><?php esc_html_e('Import content', 'furgan-toolkit'); ?></h4>
                                            <div class="control-inner">
                                                <div class="group-control">
                                                    <?php foreach ($this->item_import as $keys => $item) : ?>
                                                        <label for="<?php echo esc_attr($keys); ?>-<?php echo $key; ?>">
                                                            <input
                                                                id="<?php echo esc_attr($keys); ?>-<?php echo $key; ?>"
                                                                type="checkbox"
                                                                class="<?php echo esc_attr($keys); ?>"
                                                                value="<?php echo $key; ?>" checked disabled>
                                                            <?php echo esc_html($item); ?>
                                                        </label>
                                                    <?php endforeach; ?>
                                                    <button data-id="<?php echo $key; ?>"
                                                            data-optionid="<?php echo $key; ?>"
                                                            class="button button-primary kt-button-import">Install
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="progress-wapper">
                                            <div class="progress-item">
                                                <?php foreach ($this->item_import as $keys => $item) : ?>
                                                    <div class="meter item <?php echo esc_attr($keys); ?>">
                                                        <?php echo esc_html($item); ?>
                                                        <div class="checkmark">
                                                            <div class="checkmark_stem"></div>
                                                            <div class="checkmark_kick"></div>
                                                        </div>
                                                        <span style="width: 100%"></span>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                            <div class="progress-circle">
                                                <div class="c100 p0 dark green" data-percent="1">
                                                    <span class="percent">0%</span>
                                                    <div class="slice">
                                                        <div class="bar"></div>
                                                        <div class="fill"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php else: ?>
                    <p><?php esc_html_e('No data import', 'furgan-toolkit'); ?></p>
                <?php endif; ?>
            </div>
            <?php
        }

        public function import_theme_options()
        {
            $optionid = isset($_POST['optionid']) ? $_POST['optionid'] : "";
            if ($optionid != "") {
                $demo = $this->data_demos[$optionid];
                if (!is_array($demo)) {
                    return;
                }
            }
            if (isset($demo['theme_option']) && $demo['theme_option'] != "") {
                $data = file_get_contents($demo['theme_option']);
                update_option('_cs_options', cs_decode_string($data));
            }

            wp_die();
        }

        public function import_widget()
        {
            $optionid = isset($_POST['optionid']) ? $_POST['optionid'] : "";
            if ($optionid == "") {
                return;
            }
            $url = $this->data_demos[$optionid]['widget_path'];
            $data = file_get_contents($url);
            $data = json_decode($data);
            global $wp_registered_sidebars;
            if (empty($data) || !is_object($data)) {
                wp_die();
            }
            update_option('sidebars_widgets', array(false));
            do_action('wie_before_import');
            $data = apply_filters('wie_import_data', $data);
            $available_widgets = $this->available_widgets();
            $widget_instances = array();
            foreach ($available_widgets as $widget_data) {
                $widget_instances[$widget_data['id_base']] = get_option('widget_' . $widget_data['id_base']);
            }
            $results = array();
            foreach ($data as $sidebar_id => $widgets) {
                if ('wp_inactive_widgets' == $sidebar_id) {
                    continue;
                }
                if (isset($wp_registered_sidebars[$sidebar_id])) {
                    $sidebar_available = true;
                    $use_sidebar_id = $sidebar_id;
                    $sidebar_message_type = 'success';
                    $sidebar_message = '';
                } else {
                    $sidebar_available = false;
                    $use_sidebar_id = 'wp_inactive_widgets'; // add to inactive if sidebar does not exist in theme
                    $sidebar_message_type = 'error';
                    $sidebar_message = __('Sidebar does not exist in theme (using Inactive)', 'furgan-toolkit');
                }
                $results[$sidebar_id]['name'] = !empty($wp_registered_sidebars[$sidebar_id]['name']) ? $wp_registered_sidebars[$sidebar_id]['name'] : $sidebar_id; // sidebar name if theme supports it; otherwise ID
                $results[$sidebar_id]['message_type'] = $sidebar_message_type;
                $results[$sidebar_id]['message'] = $sidebar_message;
                $results[$sidebar_id]['widgets'] = array();
                foreach ($widgets as $widget_instance_id => $widget) {
                    $fail = false;
                    $id_base = preg_replace('/-[0-9]+$/', '', $widget_instance_id);
                    $instance_id_number = str_replace($id_base . '-', '', $widget_instance_id);
                    if (!$fail && !isset($available_widgets[$id_base])) {
                        $fail = true;
                        $widget_message_type = 'error';
                        $widget_message = __('Site does not support widget', 'furgan-toolkit');
                    }
                    $widget = apply_filters('wie_widget_settings', $widget);
                    $widget = json_decode(json_encode($widget), true);
                    $widget = apply_filters('wie_widget_settings_array', $widget);
                    if (!$fail && isset($widget_instances[$id_base])) {
                        $sidebars_widgets = get_option('sidebars_widgets');
                        $sidebar_widgets = isset($sidebars_widgets[$use_sidebar_id]) ? $sidebars_widgets[$use_sidebar_id] : array();
                        $single_widget_instances = !empty($widget_instances[$id_base]) ? $widget_instances[$id_base] : array();
                        foreach ($single_widget_instances as $check_id => $check_widget) {
                            if (in_array("$id_base-$check_id", $sidebar_widgets) && (array)$widget == $check_widget) {
                                $fail = true;
                                $widget_message_type = 'warning';
                                $widget_message = __('Widget already exists', 'furgan-toolkit');
                                break;
                            }
                        }
                    }
                    if (!$fail) {
                        $single_widget_instances = get_option('widget_' . $id_base);
                        $single_widget_instances = !empty($single_widget_instances) ? $single_widget_instances : array('_multiwidget' => 1);
                        $single_widget_instances[] = $widget;
                        end($single_widget_instances);
                        $new_instance_id_number = key($single_widget_instances);
                        if ('0' === strval($new_instance_id_number)) {
                            $new_instance_id_number = 1;
                            $single_widget_instances[$new_instance_id_number] = $single_widget_instances[0];
                            unset($single_widget_instances[0]);
                        }
                        if (isset($single_widget_instances['_multiwidget'])) {
                            $multiwidget = $single_widget_instances['_multiwidget'];
                            unset($single_widget_instances['_multiwidget']);
                            $single_widget_instances['_multiwidget'] = $multiwidget;
                        }
                        update_option('widget_' . $id_base, $single_widget_instances);
                        $sidebars_widgets = get_option('sidebars_widgets');
                        $new_instance_id = $id_base . '-' . $new_instance_id_number;
                        $sidebars_widgets[$use_sidebar_id][] = $new_instance_id;
                        update_option('sidebars_widgets', $sidebars_widgets);
                        $after_widget_import = array(
                            'sidebar' => $use_sidebar_id,
                            'sidebar_old' => $sidebar_id,
                            'widget' => $widget,
                            'widget_type' => $id_base,
                            'widget_id' => $new_instance_id,
                            'widget_id_old' => $widget_instance_id,
                            'widget_id_num' => $new_instance_id_number,
                            'widget_id_num_old' => $instance_id_number,
                        );
                        do_action('wie_after_widget_import', $after_widget_import);
                        if ($sidebar_available) {
                            $widget_message_type = 'success';
                            $widget_message = __('Imported', 'furgan-toolkit');
                        } else {
                            $widget_message_type = 'warning';
                            $widget_message = __('Imported to Inactive', 'furgan-toolkit');
                        }
                    }
                    $results[$sidebar_id]['widgets'][$widget_instance_id]['name'] = isset($available_widgets[$id_base]['name']) ? $available_widgets[$id_base]['name'] : $id_base; // widget name or ID if name not available (not supported by site)
                    $results[$sidebar_id]['widgets'][$widget_instance_id]['title'] = !empty($widget['title']) ? $widget['title'] : __('No Title', 'furgan-toolkit'); // show "No Title" if widget instance is untitled
                    $results[$sidebar_id]['widgets'][$widget_instance_id]['message_type'] = $widget_message_type;
                    $results[$sidebar_id]['widgets'][$widget_instance_id]['message'] = $widget_message;
                }
            }
            do_action('wie_after_import');
            wp_die();
        }

        public function available_widgets()
        {
            global $wp_registered_widget_controls;
            $widget_controls = $wp_registered_widget_controls;
            $available_widgets = array();
            foreach ($widget_controls as $widget) {
                if (!empty($widget['id_base']) && !isset($available_widgets[$widget['id_base']])) { // no dupes
                    $available_widgets[$widget['id_base']]['id_base'] = $widget['id_base'];
                    $available_widgets[$widget['id_base']]['name'] = $widget['name'];
                }
            }

            return apply_filters('wie_available_widgets', $available_widgets);
        }

        public function import_revslider()
        {
            $optionid = isset($_POST['optionid']) ? $_POST['optionid'] : "";
            if ($optionid == "") {
                return;
            }

            $rev_directory = '';

            if (class_exists('UniteFunctionsRev') && class_exists('ZipArchive')) {
                $rev_directory = $this->data_demos[$optionid]['revslider_path'];
            }
            if ($rev_directory != '') {

                $rev_files = array();
                $response = array();

                if (version_compare(RS_REVISION, '6.0.0', '<')) {
                    $slider = new RevSlider();
                } else {
                    $slider = new RevSliderSliderImport();

                }

                foreach (glob($rev_directory . '*.zip') as $filename) {
                    $filename = basename($filename);
                    $rev_files[] = $rev_directory . $filename;
                }

                foreach ($rev_files as $index => $rev_file) {
                    if (version_compare(RS_REVISION, '6.0.0', '<')) {
                        $response[] = $slider->importSliderFromPost(true, true, $rev_file);
                    } else {
                        $response[] = $slider->import_slider(true, $rev_file);
                    }
                }

            }
        }

        public function import_config()
        {
            $optionid = isset($_POST['optionid']) ? $_POST['optionid'] : "";
            if ($optionid != "") {
                $demo = $this->data_demos[$optionid];
                if (!empty($demo)) {
                    if (is_array($demo)) {
                        $this->update_woocommerce();
                        $this->update_mc4wp();
                        $this->update_elementor();
                        $this->delete_post($demo);
                        $this->update_menu_locations($demo);
                        $this->update_custom_metabox($demo);
                        $this->update_default_template($demo);
                        $this->update_compare($demo);
                        $this->update_settings_options($demo);
                    }
                }
            }
            wp_die();
        }

        public function update_woocommerce()
        {
            if (class_exists('WooCommerce')) {
                //Guest checkout
                update_option('woocommerce_enable_guest_checkout', 'no');
                update_option('woocommerce_enable_checkout_login_reminder', 'yes');
                //Account creation
                update_option('woocommerce_enable_signup_and_login_from_checkout', 'yes');
                update_option('woocommerce_enable_myaccount_registration', 'yes');
                update_option('woocommerce_registration_generate_username', 'yes');
                update_option('woocommerce_registration_generate_password', 'no');
            }
        }

        public function update_elementor()
        {
            if (class_exists('Elementor\Plugin')) {
                $cpt_support = get_option('elementor_cpt_support', ['page', 'post']);
                $cpt_support[] = 'product';
                $cpt_support[] = 'furgan_menu';
                $cpt_support[] = 'furgan_footer';

                update_option('elementor_cpt_support', $cpt_support);
                update_option('elementor_disable_color_schemes', 'yes');
                update_option('elementor_disable_typography_schemes', 'yes');
                update_option('elementor_load_fa4_shim', 'yes');
                update_option('elementor_experiment-e_dom_optimization', 'inactive');

                $manager = new Elementor\Core\Files\Manager();
                $manager->clear_cache();
            }
        }

        public function update_mc4wp()
        {
            if (function_exists('_mc4wp_load_plugin')) {
                update_option('mc4wp',
                    array(
                        'api_key' => 'c54ccc8b7db07f134e232bb0832dc255-us19',
                    )
                );
                $args = array(
                    'post_type' => 'mc4wp-form',
                    'posts_per_page' => 1,
                    'post_status' => 'publish',
                );
                $loop = new wp_query($args);
                if ($loop->have_posts()) {
                    while ($loop->have_posts()) {
                        $loop->the_post();
                        update_option('mc4wp_default_form_id', get_the_ID());
                    }
                }
            }
        }

        public function delete_post($demo)
        {
            foreach ($demo['delete_post'] as $key) {
                if (get_page_by_path($key, OBJECT, array('post', 'page'))) {
                    wp_delete_post((int)get_page_by_path($key, OBJECT, array('post', 'page'))->ID);
                }
            }
        }

        public function update_menu_locations($demo)
        {
            $megamenu_list = array();
            $args = array(
                'post_type' => 'furgan_menu',
                'posts_per_page' => -1,
                'post_status' => 'publish',
            );
            $loop = new wp_query($args);
            if ($loop->have_posts()) {
                while ($loop->have_posts()) {
                    $loop->the_post();
                    $megamenu_list[get_the_title()] = get_the_ID();
                }
            }

            $home_url = get_home_url();
            $menu_location = array();
            $menus = wp_get_nav_menus();
            if ($menus) {
                foreach ($menus as $menu) {
                    foreach ($demo['menu_locations'] as $key => $value) {
                        if ($menu->name == $value) {
                            $menu_location[$key] = $menu->term_id;
                        }
                    }
                    $items = wp_get_nav_menu_items($menu->term_id);
                    if (!empty($items)) {
                        foreach ($items as $item) {
                            $_menu_item_url = get_post_meta($item->ID, '_menu_item_url', true);
                            if (!empty($_menu_item_url)) {
                                $_menu_item_url = str_replace(base64_decode('aHR0cHM6Ly9kcmVhbWluZ3RoZW1lLmtpZW5kYW90YWMuY29tL2Z1cmdhbg=='), $home_url, $_menu_item_url);
                                $_menu_item_url = str_replace(base64_decode('aHR0cDovL2RyZWFtaW5ndGhlbWUua2llbmRhb3RhYy5jb20vZnVyZ2Fu'), $home_url, $_menu_item_url);
                                update_post_meta($item->ID, '_menu_item_url', $_menu_item_url);
                            }
                            if ($item->post_title == 'Shop') {
                                $demo['megamenu']['menu_content_id'] = !empty($megamenu_list['Shop']) ? $megamenu_list['Shop'] : '';
                                $demo['megamenu']['menu_width'] = '';
                                $demo['megamenu']['menu_bg'] = '';
                                $demo['megamenu']['bg_position'] = 'center';
                                update_post_meta($item->ID, '_furgan_menu_settings', maybe_unserialize($demo['megamenu']));
                            }
                            if ($item->post_title == 'Elements') {
                                $demo['megamenu']['menu_width'] = '';
                                $demo['megamenu']['menu_content_id'] = !empty($megamenu_list['Elements']) ? $megamenu_list['Elements'] : '';
                                $demo['megamenu']['menu_bg'] = '';
                                update_post_meta($item->ID, '_furgan_menu_settings', maybe_unserialize($demo['megamenu']));
                            }
                            if ($item->post_title == 'Blog') {
                                add_filter('https_ssl_verify', '__return_false');
                                $blog_bg = media_sideload_image(get_template_directory_uri() . '/assets/images/blog-bg.jpg', 0, null, 'id');
                                $demo['megamenu']['menu_content_id'] = !empty($megamenu_list['Blog']) ? $megamenu_list['Blog'] : '';
                                $demo['megamenu']['menu_width'] = '800';
                                $demo['megamenu']['menu_bg'] = !empty($blog_bg) ? $blog_bg : '';
                                $demo['megamenu']['bg_position'] = 'right';
                                update_post_meta($item->ID, '_furgan_menu_settings', maybe_unserialize($demo['megamenu']));
                            }
                        }
                    }
                }
            }
            set_theme_mod('nav_menu_locations', $menu_location);
        }

        public function update_custom_metabox($demo)
        {
            foreach ($demo['custom_metabox'] as $key => $value) {
                if (get_page_by_path($key)) {
                    update_post_meta(get_page_by_path($key)->ID, '_custom_metabox_theme_options', $value);
                }
            }
        }

        public function update_default_template($demo)
        {
            foreach ($demo['default_template'] as $key) {
                if (get_page_by_path($key)) {
                    wp_update_post(array(
                        'ID' => (int)get_page_by_path($key)->ID,
                        'comment_status' => 'closed'
                    ));
                    update_post_meta(get_page_by_path($key)->ID, '_custom_page_side_options', array(
                        'page_sidebar_layout' => 'full',
                        'page_sidebar' => 'widget-area',
                        'page_extra_class' => ''
                    ));
                }
            }
        }

        public function update_compare($demo)
        {
            if (get_page_by_path('compare')) {
                $demo['compare']['compare_page'] = get_page_by_path('compare')->ID;
                update_option('dreaming_wccp_all_settings', $demo['compare']);
            }
        }

        public function update_settings_options($demo)
        {

            update_option('show_on_front', 'page');
            $home = get_page_by_path($demo['settings_options']['home'])->ID;
            $blog = get_page_by_path($demo['settings_options']['blog'])->ID;
            if ($home) {
                update_option('page_on_front', $home);
            }
            if ($blog) {
                update_option('page_for_posts', $blog);
            }
            $blogname = $demo['settings_options']['blogname'];
            $blogdescription = $demo['settings_options']['blogdescription'];
            update_option('blogname', $blogname);
            update_option('blogdescription', $blogdescription);

            // Prepare WordPress Rewrite object in case it hasn't been initialized yet
            if (empty($wp_rewrite) || !($wp_rewrite instanceof WP_Rewrite)) {
                $wp_rewrite = new WP_Rewrite();
            }
            // Update permalink structure
            $permalink_structure = '/%postname%/';
            $wp_rewrite->set_permalink_structure($permalink_structure);
            // Recreate rewrite rules
            $wp_rewrite->flush_rules();
        }
    }

    new FURGAN_IMPORTER();
}